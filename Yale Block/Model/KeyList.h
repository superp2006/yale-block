//
//  KeyList.h
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface KeyList : NSManagedObject

@property (nonatomic, retain) NSString * lockName;
@property (nonatomic, retain) NSString * keyID;
@property (nonatomic, retain) NSString * keyName;
@property (nonatomic, retain) NSString * keyType;
@property (nonatomic, retain) NSNumber * keyIndex;
@property (nonatomic, retain) NSString * lockID;
@property (nonatomic, retain) NSString * masterKeyID;

@end
