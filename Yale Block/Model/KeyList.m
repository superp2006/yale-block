//
//  KeyList.m
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "KeyList.h"


@implementation KeyList

@dynamic lockName;
@dynamic keyID;
@dynamic keyName;
@dynamic keyType;
@dynamic keyIndex;
@dynamic lockID;
@dynamic masterKeyID;

@end
