//
//  LockData.h
//  Yale Block
//
//  Created by Peter on 10/14/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LockData : NSObject

@property (strong, nonatomic) NSString *bleUUID;
@property (strong, nonatomic) NSString *bleID;
@property (strong, nonatomic) NSString *beaconUUID;
@property (strong, nonatomic) NSNumber *beaconMajor;
@property (strong, nonatomic) NSNumber *beaconMinor;
@property (strong, nonatomic) NSNumber *inDatabase;

@end
