//
//  LockData.m
//  Yale Block
//
//  Created by Peter on 10/14/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "LockData.h"

@implementation LockData

@synthesize bleUUID;
@synthesize bleID;
@synthesize beaconUUID;
@synthesize beaconMajor;
@synthesize beaconMinor;
@synthesize inDatabase;

@end
