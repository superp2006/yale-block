//
//  MasterKeyInfo.m
//  Yale Block
//
//  Created by Peter on 8/18/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "MasterKeyInfo.h"
#import "GuestKeyInfo.h"
#import "LockInfo.h"


@implementation MasterKeyInfo

@dynamic encryptKey;
@dynamic keyAssigned;
@dynamic keyID;
@dynamic keyName;
@dynamic keyNumber;
@dynamic lockID;
@dynamic lockName;
@dynamic guestKeys;
@dynamic lock;

@end
