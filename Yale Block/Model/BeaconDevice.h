//
//  BeaconDevice.h
//  Yale Block
//
//  Created by Peter on 10/14/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconDevice : NSObject

@property (strong, nonatomic) NSString *beaconUUID;
@property (strong, nonatomic) NSNumber *beaconRssi;
@property (strong, nonatomic) NSNumber *beaconMajor;
@property (strong, nonatomic) NSNumber *beaconMinor;

@end
