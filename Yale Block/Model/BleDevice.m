//
//  BleDevice.m
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "BleDevice.h"

@implementation BleDevice

@synthesize deviceName;
@synthesize deviceRssi;
@synthesize deviceUUID;

@end
