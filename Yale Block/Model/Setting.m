//
//  Setting.m
//  Yale Block
//
//  Created by Peter on 8/6/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "Setting.h"


@implementation Setting

@dynamic autoUnlock;
@dynamic generateNotification;
@dynamic gestureUnlock;
@dynamic showHint;

@end
