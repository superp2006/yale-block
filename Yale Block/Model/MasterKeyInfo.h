//
//  MasterKeyInfo.h
//  Yale Block
//
//  Created by Peter on 8/18/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GuestKeyInfo, LockInfo;

@interface MasterKeyInfo : NSManagedObject

@property (nonatomic, retain) NSString * encryptKey;
@property (nonatomic, retain) NSNumber * keyAssigned;
@property (nonatomic, retain) NSString * keyID;
@property (nonatomic, retain) NSString * keyName;
@property (nonatomic, retain) NSString * keyNumber;
@property (nonatomic, retain) NSString * lockID;
@property (nonatomic, retain) NSString * lockName;
@property (nonatomic, retain) NSSet *guestKeys;
@property (nonatomic, retain) LockInfo *lock;
@end

@interface MasterKeyInfo (CoreDataGeneratedAccessors)

- (void)addGuestKeysObject:(GuestKeyInfo *)value;
- (void)removeGuestKeysObject:(GuestKeyInfo *)value;
- (void)addGuestKeys:(NSSet *)values;
- (void)removeGuestKeys:(NSSet *)values;

@end
