//
//  Beacons.m
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "Beacons.h"

@implementation Beacons

@dynamic action;
@dynamic enable;
@dynamic event;
@dynamic major;
@dynamic minor;
@dynamic name;
@dynamic uuid;

@end
