//
//  BleDevice.h
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BleDevice : NSObject

@property (nonatomic, retain) NSString *deviceName;
@property (nonatomic, retain) NSNumber *deviceRssi;
@property (nonatomic, retain) NSString *deviceUUID;

@end
