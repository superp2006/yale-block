//
//  Beacons.h
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Beacons : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSNumber * enable;
@property (nonatomic, retain) NSString * event;
@property (nonatomic, retain) NSNumber * major;
@property (nonatomic, retain) NSNumber * minor;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * uuid;

@end
