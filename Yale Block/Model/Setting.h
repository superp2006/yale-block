//
//  Setting.h
//  Yale Block
//
//  Created by Peter on 8/6/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Setting : NSManagedObject

@property (nonatomic, retain) NSNumber * autoUnlock;
@property (nonatomic, retain) NSNumber * generateNotification;
@property (nonatomic, retain) NSNumber * gestureUnlock;
@property (nonatomic, retain) NSNumber * showHint;

@end
