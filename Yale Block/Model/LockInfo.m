//
//  LockInfo.m
//  Yale Block
//
//  Created by Peter on 7/24/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "LockInfo.h"
#import "MasterKeyInfo.h"


@implementation LockInfo

@dynamic autoLockTime;
@dynamic lockID;
@dynamic lockName;
@dynamic mute;
@dynamic masterKeys;
@dynamic lockEncryptKey;
@dynamic lockNumber;
@dynamic lockUUID;

- (void)addMasterKeysObject:(MasterKeyInfo *)value {
    
    if ([self.masterKeys containsObject:value]) {
        return;
    }
    // Use NSManagedObject's methods for inserting an object
    [[self mutableOrderedSetValueForKey:@"masterKeys"] addObject:value];
}

@end

