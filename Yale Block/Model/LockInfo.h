//
//  LockInfo.h
//  Yale Block
//
//  Created by Peter on 7/24/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MasterKeyInfo;

@interface LockInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * autoLockTime;
@property (nonatomic, retain) NSString * lockID;
@property (nonatomic, retain) NSString * lockName;
@property (nonatomic, retain) NSNumber * mute;
@property (nonatomic, retain) NSOrderedSet *masterKeys;
@property (nonatomic, retain) NSString *lockEncryptKey;
@property (nonatomic, retain) NSNumber *lockNumber;
@property (nonatomic, retain) NSString *lockUUID;
@end

@interface LockInfo (CoreDataGeneratedAccessors)

- (void)insertObject:(MasterKeyInfo *)value inMasterKeysAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMasterKeysAtIndex:(NSUInteger)idx;
- (void)insertMasterKeys:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMasterKeysAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMasterKeysAtIndex:(NSUInteger)idx withObject:(MasterKeyInfo *)value;
- (void)replaceMasterKeysAtIndexes:(NSIndexSet *)indexes withMasterKeys:(NSArray *)values;
- (void)addMasterKeysObject:(MasterKeyInfo *)value;
- (void)removeMasterKeysObject:(MasterKeyInfo *)value;
- (void)addMasterKeys:(NSOrderedSet *)values;
- (void)removeMasterKeys:(NSOrderedSet *)values;
@end
