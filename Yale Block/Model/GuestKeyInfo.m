//
//  GuestKeyInfo.m
//  Yale Block
//
//  Created by Peter on 8/18/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "GuestKeyInfo.h"
#import "MasterKeyInfo.h"


@implementation GuestKeyInfo

@dynamic encryptKey;
@dynamic endTime;
@dynamic keyID;
@dynamic keyName;
@dynamic keyNumber;
@dynamic lockID;
@dynamic masterKeyID;
@dynamic startTime;
@dynamic masterKey;

@end
