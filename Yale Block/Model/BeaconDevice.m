//
//  BeaconDevice.m
//  Yale Block
//
//  Created by Peter on 10/14/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "BeaconDevice.h"

@implementation BeaconDevice

@synthesize beaconUUID;
@synthesize beaconRssi;
@synthesize beaconMajor;
@synthesize beaconMinor;

@end
