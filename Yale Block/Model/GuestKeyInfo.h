//
//  GuestKeyInfo.h
//  Yale Block
//
//  Created by Peter on 8/18/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MasterKeyInfo;

@interface GuestKeyInfo : NSManagedObject

@property (nonatomic, retain) NSString * encryptKey;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, retain) NSString * keyID;
@property (nonatomic, retain) NSString * keyName;
@property (nonatomic, retain) NSString * keyNumber;
@property (nonatomic, retain) NSString * lockID;
@property (nonatomic, retain) NSString * masterKeyID;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) MasterKeyInfo *masterKey;

@end
