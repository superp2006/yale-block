//
//  SettingTableViewController.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "SettingTableViewController.h"
#import "Setting.h"

@interface SettingTableViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *showHintSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *gestureUnlockSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *lockNotificationSwitch;
@property (weak, nonatomic) IBOutlet UISlider *autoUnlockSlider;


@end

@implementation SettingTableViewController
@synthesize managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
    
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    Setting *setting = [fetchedObjects objectAtIndex:0];
    
    [self.lockNotificationSwitch setOn:[setting.generateNotification boolValue] animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
