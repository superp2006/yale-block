//
//  TabBarViewController.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "TabBarViewController.h"
#import "IonIcons.h"
@interface TabBarViewController ()

@end

@implementation TabBarViewController
@synthesize managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITabBar *tabBar = self.tabBar;
    
    UITabBarItem *homeTabBarItem = [tabBar.items objectAtIndex:0];
    UITabBarItem *keyTabBarItem = [tabBar.items objectAtIndex:1];
    UITabBarItem *lockTabBarItem = [tabBar.items objectAtIndex:2];
    UITabBarItem *recordTabBarItem = [tabBar.items objectAtIndex:3];
    UITabBarItem *settingTabBarItem = [tabBar.items objectAtIndex:4];
    [settingTabBarItem setEnabled:FALSE];
    [keyTabBarItem setEnabled:FALSE];
    [recordTabBarItem setEnabled:FALSE];
    
    homeTabBarItem.image = [[IonIcons imageWithIcon:ion_home
                                         iconColor:[UIColor grayColor]
                                          iconSize:30.0f
                                         imageSize:CGSizeMake(30.0f, 30.0f)]
                            imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    keyTabBarItem.image = [[IonIcons imageWithIcon:ion_key
                                        iconColor:[UIColor grayColor]
                                         iconSize:30.0f
                                        imageSize:CGSizeMake(30.0f, 30.0f)]
                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    lockTabBarItem.image = [[IonIcons imageWithIcon:ion_lock_combination
                                         iconColor:[UIColor grayColor]
                                          iconSize:30.0f
                                         imageSize:CGSizeMake(30.0f, 30.0f)]
                            imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    recordTabBarItem.image = [[IonIcons imageWithIcon:ion_clipboard
                                           iconColor:[UIColor grayColor]
                                            iconSize:30.0f
                                           imageSize:CGSizeMake(30.0f, 30.0f)]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    settingTabBarItem.image = [[IonIcons imageWithIcon:ion_gear_b
                                            iconColor:[UIColor grayColor]
                                             iconSize:30.0f
                                            imageSize:CGSizeMake(30.0f, 30.0f)]
                               imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    homeTabBarItem.selectedImage = [IonIcons imageWithIcon:ion_home
                                                 iconColor:[UIColor redColor]
                                                  iconSize:30.0f
                                                 imageSize:CGSizeMake(30.0f, 30.0f)];
    
    keyTabBarItem.selectedImage = [IonIcons imageWithIcon:ion_key
                                                iconColor:[UIColor redColor]
                                                 iconSize:30.0f
                                                imageSize:CGSizeMake(30.0f, 30.0f)];
    
    lockTabBarItem.selectedImage = [IonIcons imageWithIcon:ion_lock_combination
                                                 iconColor:[UIColor redColor]
                                                  iconSize:30.0f
                                                 imageSize:CGSizeMake(30.0f, 30.0f)];
    
    recordTabBarItem.selectedImage = [IonIcons imageWithIcon:ion_clipboard
                                                   iconColor:[UIColor redColor]
                                                    iconSize:30.0f
                                                   imageSize:CGSizeMake(30.0f, 30.0f)];
    
    settingTabBarItem.selectedImage = [IonIcons imageWithIcon:ion_gear_b
                                                    iconColor:[UIColor redColor]
                                                     iconSize:30.0f
                                                    imageSize:CGSizeMake(30.0f, 30.0f)];

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]}
                                             forState:UIControlStateSelected];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
