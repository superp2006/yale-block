//
//  RecordTableViewController.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "RecordTableViewController.h"
#define kCancelButton 0
#define kKeepOneMonth 1
#define kClearAll 2

@interface RecordTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *clearButton;
@end

@implementation RecordTableViewController {
    NSArray *recordList;
    NSArray *sectionList;
}

@synthesize managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    recordList = @[@"No record found"];
    sectionList = @[@"Record"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return sectionList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return recordList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [recordList objectAtIndex:indexPath.row];
    
    return cell;
}

- (IBAction)clearButtonClick:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Clear Lock Records" message:@"\n\nClear lock records\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Keep 1 month records",@"Clear All", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    switch (buttonIndex) {
        case kCancelButton:
            NSLog(@"Cancel button clicked");
            break;
            
        case kClearAll:
            NSLog(@"Clear All button pressed");
            break;
            
        case kKeepOneMonth:
            NSLog(@"Keep One Month pressed");
            break;
    }

}
@end
