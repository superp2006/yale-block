//
//  HomeTableViewController.h
//  Yale Block
//
//  Created by Peter on 10/15/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeLockService.h"
#import "LeDiscovery.h"
#import "BeaconManage.h"

@interface HomeTableViewController : UITableViewController <LeDiscoveryDelegate, LeLockServiceProtocol, BeaconManageDelegate>

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
//@property (nonatomic,strong) CMMotionManager *motionManager;
@property (strong, nonatomic) IBOutlet UITableView *homeTableView;

@end
