//
//  HomeTableViewController.m
//  Yale Block
//
//  Created by Peter on 10/15/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "HomeTableViewController.h"
#import "IonIcons.h"
#import "BleDevice.h"
#import "BeaconDevice.h"
#import "LockData.h"
#import "CustomHomeTableViewCell.h"
#import "AppDelegate.h"

@interface HomeTableViewController () {
    
}

@property (nonatomic) NSTimer *checkTimer;

@property (nonatomic) double xData, yData, zData;
@property (assign) double lastXdata;
@property (assign) double lastYdata;
@property (assign) double lastZdata;
@property (assign) double pastX1,pastX2,pastX3;
@property (assign) int resetXcnt;
@property BOOL isInBackground;
@property BOOL resetOpenXLock;
@property BOOL resetOpenYLock;

@end

@implementation HomeTableViewController

@synthesize managedObjectContext;
//@synthesize motionManager;
@synthesize checkTimer;
@synthesize xData, yData, zData;
@synthesize lastXdata;
@synthesize lastYdata;
@synthesize lastZdata;
@synthesize pastX1, pastX2, pastX3;
@synthesize resetXcnt;
@synthesize isInBackground;
@synthesize resetOpenXLock;
@synthesize resetOpenYLock;

- (void)viewDidLoad {
    [super viewDidLoad];

    NSLog(@"HomeTableViewController ******************");
    // Do any additional setup after loading the view.
    isInBackground = FALSE;
    resetOpenXLock = FALSE;
    resetOpenYLock = FALSE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadBeacon) name:@"ReloadBeacon" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInBackground:) name:@"EnterBackground" object:nil];

    checkTimer = [NSTimer timerWithTimeInterval:0.5 target:self selector:@selector(scheduleEvent) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:checkTimer forMode:NSRunLoopCommonModes];
    
//    motionManager = nil;
//    motionManager = [[CMMotionManager alloc] init];
//    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
//        
//        self.xData = accelerometerData.acceleration.x;
//        self.yData = accelerometerData.acceleration.y;
//        self.zData = accelerometerData.acceleration.z;
//        NSLog(@"x: %f | y: %f | z: %f", self.xData, self.yData, self.zData);
//        
//    }];

    
    [self.homeTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillAppear");
    [[LeDiscovery sharedInstance] setDiscoveryDelegate:self];
    [[LeDiscovery sharedInstance] setPeripheralDelegate:self];
    [[LeDiscovery sharedInstance] setIsInHome:TRUE];
    
    [[BeaconManage sharedInstance] setBeaconDelegate:self];
    [[BeaconManage sharedInstance] setIsInHome:TRUE];
    [self.homeTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:nil]];
    }
    
    [[LeDiscovery sharedInstance] setIsInHome:FALSE];
    [[BeaconManage sharedInstance] setIsInHome:FALSE];
}

- (void)appInBackground:(NSNotification *) notification {
    NSDictionary *info = notification.userInfo;
    NSNumber *inBackground = info[@"background"];
    NSLog(@"inBackground: %s", [inBackground boolValue] ? "true" : "false");
    isInBackground = [inBackground boolValue];
    [[LeDiscovery sharedInstance] setIsInBackground:[inBackground boolValue]];
    
    NSArray *lockServices = [[LeDiscovery sharedInstance] connectedServices];
    
    for (LeLockService *lockService in lockServices) {
        [[LeDiscovery sharedInstance] disconnectPeripheral:lockService.peripheral];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scheduleEvent {
    AppDelegate * AppDlg=(AppDelegate *) [[UIApplication sharedApplication] delegate];
    BOOL isOpenLock = [[LeDiscovery sharedInstance] isOpenLock];
    
    
    if (isInBackground) {
        
        pastX3 = pastX2;
        pastX2 = pastX1;
        pastX1 = lastXdata;
        lastXdata = xData;
        lastYdata = yData;
        lastZdata = zData;
        xData = AppDlg.xData;
        yData = AppDlg.yData;
        zData = AppDlg.zData;
        NSLog(@"x=%f y=%f z=%f lastz=%f rCnt=%d rX=%d openlock=%d",xData, yData, zData, lastZdata, resetXcnt, resetOpenXLock, isOpenLock);
        
        if (yData < 0.2 && yData > -0.3 && lastXdata < -0.3 && pastX1 < -0.3 && resetXcnt == 0) {
            resetOpenXLock = TRUE;
            resetOpenYLock = FALSE;
            resetXcnt=15;
        }
        
        if (yData < -0.5 || lastYdata < -0.5) {
            resetXcnt = 0;
            resetOpenXLock = FALSE;
        }
        
        if (yData < -0.6) {
            resetOpenYLock = TRUE;
            resetOpenXLock = FALSE;
        }
        
        if (resetOpenXLock && (zData - lastZdata) > 1.45 && !isOpenLock) {
            NSLog(@"scheduleEvent: unlockDoor");
            resetOpenYLock = FALSE;
            resetOpenXLock = FALSE;
            
            [[LeDiscovery sharedInstance] setIsOpenLock:TRUE];
            [AppDlg playAlarm];
//            NSString *message = @"Unlocking door....";
//            [self displayNotificationOnScreen:message];
            
//            NSArray *serviceList = [[LeDiscovery sharedInstance] connectedServices];
//            if (serviceList.count > 0) {
//                NSLog(@"scheduleEvent UnlockDoor");
//                LeLockService *lockService = serviceList.firstObject;
//                [lockService unlockDoor];
//                resetOpenYLock = FALSE;
//                resetOpenXLock = FALSE;
//            }
        }
        
        if(yData > 0.7) {
            resetOpenXLock = FALSE;
            resetOpenYLock = FALSE;
        }
        
        if(resetXcnt > 0){
            resetXcnt--; if(resetXcnt==0) {
                resetOpenXLock = FALSE;
                pastX3 = 0.4;
                pastX2 = 0.4;
                pastX3 = 0.4;
                resetXcnt=3;
            }
        }

    }
}


#pragma mark - BeaconManageDelegate

- (void)discoveryDidRefresh {
    NSLog(@"discoveryDidRefresh");
    [self.homeTableView reloadData];
}

- (void)reloadTable {
    [self.homeTableView reloadData];
}

- (void)reloadBeacon {
    NSLog(@"reloadBeacon");
    [[BeaconManage sharedInstance] startMonitoringBeacon];
}

- (void)getBleDeviceData {
    NSLog(@"getBleDeviceData");
    [[BeaconManage sharedInstance] setBleDeviceList:[[LeDiscovery sharedInstance] bleDeviceList]];
    [[BeaconManage sharedInstance] loadLockData];
}

#pragma mark - LeLockServiceProtocol

- (void)unLockDoorInBackground {
    [[LeDiscovery sharedInstance] setIsCloseLockTimeStarted:TRUE];
    [[LeDiscovery sharedInstance] setIsOpenLock:FALSE];
}

- (void)beaconWithinRange:(NSString *)bleUUID {
    NSLog(@"beaconWithinRange");
//    [[LeDiscovery sharedInstance] disconnectBleDevice:bleUUID];
    [[LeDiscovery sharedInstance] connectBleDevice:bleUUID];
}

- (void)didUnlockDoor:(NSString *)bleUUID lockName:(NSString *)lockName {
    NSLog(@"didUnlockDoor");
    NSString *message = @" is unlocked";
    [self displayNotificationOnScreen:[lockName stringByAppendingString:message]];
    [[LeDiscovery sharedInstance] disconnectBleDevice:bleUUID];
}

- (void)didLockDoor:(NSString *)bleUUID lockName:(NSString *)lockName {
    NSLog(@"didLockDoor");
    NSString *message = @" is locked";
    [self displayNotificationOnScreen:[lockName stringByAppendingString:message]];
    [[LeDiscovery sharedInstance] setIsCloseLock:FALSE];
    [[LeDiscovery sharedInstance] disconnectBleDevice:bleUUID];
}

- (void)disconnectLockNow:(NSString *)bleUUID {
    NSLog(@"disconnectLockNow");
    [[LeDiscovery sharedInstance] disconnectBleDevice:bleUUID];
}

#pragma mark - displayNotification

- (void)displayNotificationOnScreen:(NSString *)message {
    NSString *stng;
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"HH:mm:ss"];
    
    stng = [NSString stringWithFormat:@"[ %@ ]\n%@",[formatter stringFromDate:[NSDate date]], message];
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    notification.alertAction = @"Show";
    notification.alertBody = stng;
    notification.hasAction = NO;
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    notification.timeZone = [NSTimeZone defaultTimeZone];
//    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:notification]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSArray *allLockSevices = [[LeDiscovery sharedInstance] connectedServices];
    if (allLockSevices.count <= 0) {
        //create a lable size to fit the Table View
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                                        self.tableView.bounds.size.width,
                                                                        self.tableView.bounds.size.height)];
        //set the message
        messageLbl.text = @"No lock within range";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        //auto size the text
        [messageLbl sizeToFit];
        
        //set back to label view
        self.tableView.backgroundView = messageLbl;
        //no separator
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    } else {
        self.tableView.backgroundView = nil;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSInteger count = 0;
    
//    NSArray *allLockData = [[BeaconManage sharedInstance] lockDataList];
//    
//    for (LockData *lockData in allLockData) {
//        if ([lockData.inDatabase boolValue]) {
//            count++;
//        }
//    }
    
    NSArray *allLockServices = [[LeDiscovery sharedInstance] connectedServices];
    NSLog(@"numberOfRowInSection: %zd", allLockServices.count);
    
    return allLockServices.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CustomHomeTableViewCell *cell = (CustomHomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HomeCell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(CustomHomeTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSArray *serviceList = [[LeDiscovery sharedInstance] connectedServices];
//    if (serviceList.count > 0) {
        LeLockService *lockService = serviceList[indexPath.row];
        
    [cell setupView:nil];
    [[cell lockNameLabel] setText:lockService.lockName];
    
    [cell setDidTapLockButtonBlock:^{
        [lockService lockDoor];
    }];
    
    [cell setDidTapUnlockButtonBlock:^{
        [lockService unlockDoor];
    }];
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150.0f;
}

@end
