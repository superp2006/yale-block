//
//  TabBarViewController.h
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@end
