//
//  NewGuestKeyTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "NewGuestKeyTableViewController.h"
#import "MasterKeyInfo.h"
#import "GuestKeyInfo.h"
#import "LockInfo.h"
#import "KeyList.h"

#define kLockPickerSwitch 1
#define kLockPickerIndex 2

#define kMasterKeyPickerSwitch 3
#define kMasterKeyPickerIndex 4

#define kKeyPickerSwitch 5
#define kKeyPickerIndex 6

#define kStartTimePickerSwitch 7
#define kStartTimePickerIndex 8

#define kEndTimePickerSwitch 9
#define kEndTimePickerIndex 10

#define kKeyPickerTag 99
#define kLockPickerTag 98
#define kMasterKeyPickerTag 97

#define kPickerCellHeight 180

@interface NewGuestKeyTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UITextView *encryptKeyTextView;
@property (weak, nonatomic) IBOutlet UITextField *keyNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *masterKeyPickerLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *masterKeyPicker;

@property (weak, nonatomic) IBOutlet UILabel *lockPickerLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *lockPicker;

@property (weak, nonatomic) IBOutlet UILabel *keyNumberPickerLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *keyNumberPicker;

@property (weak, nonatomic) IBOutlet UILabel *startTimePickerLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *startTimePicker;

@property (weak, nonatomic) IBOutlet UILabel *endTimePickerLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *endTimePicker;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (assign) BOOL pickerIsShowing;
@property (assign) NSInteger selectPickerIndex;

@property (strong, nonatomic) MasterKeyInfo *selectMasterKey;
@property (assign, nonatomic) NSString *selectLockID;
@property (assign, nonatomic) NSString *selectLockName;

@property (strong, nonatomic) NSArray *lockAssignList;
@property (strong, nonatomic) NSOrderedSet *masterKeyAssignList;
@property (strong, nonatomic) NSMutableArray *keyAssignList;

@end

@implementation NewGuestKeyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"NewGuestKeyController");
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lockID != %@", @"00000000-0000-0000-0000-000000000000"];
    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    self.lockAssignList = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    LockInfo *lockInfo = [self.lockAssignList objectAtIndex:0];
    self.selectLockID = lockInfo.lockID;
    self.selectLockName = lockInfo.lockName;
    
    self.masterKeyAssignList = [lockInfo masterKeys];
    MasterKeyInfo *masterKeyInfo = [self.masterKeyAssignList firstObject];
    self.selectMasterKey = masterKeyInfo;
    
    [self populateKeyAssignList:masterKeyInfo];
    [self.keyNumberPickerLabel setText:[self.keyAssignList objectAtIndex:0]];
    [self.lockPickerLabel setText:lockInfo.lockName];
    [self.masterKeyPickerLabel setText:masterKeyInfo.keyName];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    [self.startTimePicker setMinimumDate:[NSDate date]];
    [self.startTimePickerLabel setText:[self.dateFormatter stringFromDate:[NSDate date]]];
    
    [self.endTimePicker setMinimumDate:[NSDate date]];
    [self.endTimePickerLabel setText:[self.dateFormatter stringFromDate:[NSDate date]]];
    
    [self.lockPicker setDelegate:self];
    [self.masterKeyPicker setDelegate:self];
    [self.keyNumberPicker setDelegate:self];
    [self.keyNameTextField setDelegate:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateKeyAssignList:(MasterKeyInfo *)masterKeyInfo {
    
    [self.keyAssignList removeAllObjects];
    self.keyAssignList = [NSMutableArray arrayWithObjects:
                          @"Guest Key #1", @"Guest Key #2", @"Guest Key #3", @"Guest Key #4", @"Guest Key #5",
                          @"Guest Key #6", @"Guest Key #7", @"Guest Key #8", @"Guest Key #9", @"Guest Key #10",
                          @"Guest Key #11", @"Guest Key #12", @"Guest Key #13", @"Guest Key #14", @"Guest Key #15",
                          @"Guest Key #16", @"Guest Key #17", @"Guest Key #18", @"Guest Key #19", @"Guest Key #20",
                          nil];
    
    [[masterKeyInfo guestKeys] enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        GuestKeyInfo *keyInfo = obj;
        NSInteger removeKeyIndex;
        
        for (NSInteger i = 0; i < [self.keyAssignList count]; i++) {
            if ([keyInfo.keyNumber isEqualToString:[self.keyAssignList objectAtIndex:i]]) {
                removeKeyIndex = i;
                break;
            }
        }
        [self.keyAssignList removeObjectAtIndex:removeKeyIndex];
    }];
    
    [self.keyNumberPicker reloadAllComponents];
}

- (NSInteger)getKeyListIndex {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    return [fetchedObjects count];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.row == kLockPickerIndex || indexPath.row == kMasterKeyPickerIndex || indexPath.row == kKeyPickerIndex || indexPath.row == kStartTimePickerIndex || indexPath.row == kEndTimePickerIndex) {
        if (self.selectPickerIndex == indexPath.row) {
            height = self.pickerIsShowing ? kPickerCellHeight : 0.0f;
        } else {
            height = 0.0f;
        }
    }
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        height = 100.0f;
    }
    
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == kLockPickerSwitch || indexPath.row == kMasterKeyPickerSwitch || indexPath.row == kKeyPickerSwitch || indexPath.row == kStartTimePickerSwitch || indexPath.row == kEndTimePickerSwitch){
        self.selectPickerIndex = indexPath.row+1;
        
        if (self.pickerIsShowing){
            
            [self hidePickerCell];
            
        } else {
            
            if (indexPath.row == kMasterKeyPickerSwitch) {
                if ([self.masterKeyAssignList count] > 0) {
                    [self showPickerCell];
                }
            } else {
                [self showPickerCell];
            }
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    
}

- (void)showPickerCell {
    
    self.pickerIsShowing = YES;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    if (self.selectPickerIndex == kMasterKeyPickerIndex) {
        self.masterKeyPicker.hidden = NO;
        self.masterKeyPicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.masterKeyPicker.alpha = 1.0f;
        }];
    } else if (self.selectPickerIndex == kKeyPickerIndex) {
        self.keyNumberPicker.hidden = NO;
        self.keyNumberPicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.keyNumberPicker.alpha = 1.0f;
        }];
    } else if (self.selectPickerIndex == kStartTimePickerIndex) {
        self.startTimePicker.hidden = NO;
        self.startTimePicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.startTimePicker.alpha = 1.0f;
        }];
    } else if (self.selectPickerIndex == kEndTimePickerIndex) {
        self.endTimePicker.hidden = NO;
        self.endTimePicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.endTimePicker.alpha = 1.0f;
        }];
    } else if (self.selectPickerIndex == kLockPickerIndex) {
        self.lockPicker.hidden = NO;
        self.lockPicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.lockPicker.alpha = 1.0f;
        }];
    }

    
}

- (void)hidePickerCell {
    
    self.pickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.lockPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.lockPicker.hidden = YES;
                     }];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.masterKeyPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.masterKeyPicker.hidden = YES;
                     }];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.keyNumberPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.keyNumberPicker.hidden = YES;
                     }];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.startTimePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.startTimePicker.hidden = YES;
                     }];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.endTimePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.endTimePicker.hidden = YES;
                     }];
    
}

#pragma mark - Picker

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag == kLockPickerTag) {
        return [self.lockAssignList count];
    }
    
    if (pickerView.tag == kMasterKeyPickerTag) {
        return [self.masterKeyAssignList count];
    }
    
    if (pickerView.tag == kKeyPickerTag) {
        return [self.keyAssignList count];
    }
    
    return 0;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag == kLockPickerTag) {
        
        LockInfo *lockInfo = [self.lockAssignList objectAtIndex:row];
        
        return lockInfo.lockName;
    }
    
    if (pickerView.tag == kMasterKeyPickerTag) {
        
        MasterKeyInfo *masterKeyInfo = [self.masterKeyAssignList objectAtIndex:row];
        
        return masterKeyInfo.keyName;
    }
    
    if (pickerView.tag == kKeyPickerTag) {
        
        return [self.keyAssignList objectAtIndex:row];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (self.selectPickerIndex == kLockPickerIndex) {
        
        LockInfo *lockInfo = [self.lockAssignList objectAtIndex:row];
        [self.lockPickerLabel setText:lockInfo.lockName];
        self.selectLockID = lockInfo.lockID;
        self.masterKeyAssignList = [lockInfo masterKeys];
        MasterKeyInfo *keyInfo = [self.masterKeyAssignList objectAtIndex:0];
        [self.masterKeyPickerLabel setText:keyInfo.keyName];
    }
    
    if (self.selectPickerIndex == kMasterKeyPickerIndex) {
        
        MasterKeyInfo *masterKeyInfo = [self.masterKeyAssignList objectAtIndex:row];
        [self.masterKeyPickerLabel setText:masterKeyInfo.keyName];
        
        [self populateKeyAssignList:masterKeyInfo];
        
        [self.keyNumberPickerLabel setText:[self.keyAssignList objectAtIndex:0]];
        [self.keyNumberPicker reloadAllComponents];
    }
    
    if (self.selectPickerIndex == kKeyPickerIndex) {
        
        [self.keyNumberPickerLabel setText:[self.keyAssignList objectAtIndex:row]];
        
    }
}

#pragma mark - Action methods
- (IBAction)saveButton:(id)sender {
    GuestKeyInfo *saveGuestKey = [NSEntityDescription insertNewObjectForEntityForName:@"GuestKeyInfo" inManagedObjectContext:[self managedObjectContext]];
    
    NSUUID *uuid = [[NSUUID alloc] init];
    saveGuestKey.keyID = [uuid UUIDString];
    saveGuestKey.encryptKey = [self.encryptKeyTextView text];
    
    NSString *keyName = [self.keyNameTextField text];
    if (keyName && [keyName length]) {
        saveGuestKey.keyName = keyName;
    } else {
        keyName = [NSString stringWithFormat:@"%@ - G Key #%@", self.selectLockName, [[self.keyNumberPickerLabel text] substringFromIndex:11]];
        saveGuestKey.keyName = keyName;
    }

    saveGuestKey.keyNumber = [self.keyNumberPickerLabel text];
    saveGuestKey.lockID = self.selectLockID;
    saveGuestKey.masterKeyID = [self.selectMasterKey keyID];
    saveGuestKey.startTime = [self.dateFormatter dateFromString:[self.startTimePickerLabel text]];
    saveGuestKey.endTime = [self.dateFormatter dateFromString:[self.endTimePickerLabel text]];
    
    [self.selectMasterKey addGuestKeysObject:saveGuestKey];
    
    KeyList *keyList = [NSEntityDescription insertNewObjectForEntityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    
    keyList.keyID = [uuid UUIDString];
    keyList.keyIndex = [NSNumber numberWithInteger:[self getKeyListIndex]];
    keyList.keyName = saveGuestKey.keyName;
    keyList.keyType = @"G";
    keyList.lockName = self.selectLockName;
    keyList.lockID = saveGuestKey.lockID;
    keyList.masterKeyID = saveGuestKey.masterKeyID;
    
    NSError *error;
    if ([[self managedObjectContext] save:&error]) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your new master key could not be saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startTimePickerChanged:(UIDatePicker *)sender {
    [self.startTimePickerLabel setText:[self.dateFormatter stringFromDate:sender.date]];
    [self.endTimePicker setMinimumDate:sender.date];
    [self.endTimePickerLabel setText:[self.dateFormatter stringFromDate:sender.date]];
}

- (IBAction)endTimePickerChanged:(UIDatePicker *)sender {
    [self.endTimePickerLabel setText:[self.dateFormatter stringFromDate:sender.date]];
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
