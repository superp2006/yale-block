//
//  UpdateMasterKeyTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "UpdateMasterKeyTableViewController.h"
#import "MasterKeyInfo.h"

@interface UpdateMasterKeyTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *removeButton;
@property (weak, nonatomic) IBOutlet UITextView *encryptKeyTextView;
@property (weak, nonatomic) IBOutlet UITextField *keyNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *keyNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *lockNameLabel;

@property (strong, nonatomic) NSManagedObject *record;

@end

@implementation UpdateMasterKeyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MasterKeyInfo" inManagedObjectContext:[self managedObjectContext]];
    
    NSLog(@"keySelected: %@", self.keySelected);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyID == %@", self.keySelected];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyNumber" ascending:YES]]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    self.record = [fetchedObjects objectAtIndex:0];
    
    [self.keyNumberLabel setText:[self.record valueForKey:@"keyNumber"]];
    [self.lockNameLabel setText:[self.record valueForKey:@"lockName"]];
    [self.keyNameTextField setText:[self.record valueForKey:@"keyName"]];
    [self.encryptKeyTextView setText:[self.record valueForKey:@"encryptKey"]];
    [self.keyNameTextField setDelegate:self];
    
    if ([[self.record valueForKey:@"keyNumber"] isEqualToString:@"Master Key #1"]) {
        [self.removeButton setEnabled:NO];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"viewWillDisappear");
    
    if ([self.keySelected length] > 0) {
        NSError *error = nil;
        NSArray *fetchedObjects = [self loadKeyList:self.keySelected];
        
        [[fetchedObjects objectAtIndex:0] setValue:[self.keyNameTextField text] forKey:@"keyName"];
        
        [self.record setValue:[self.keyNameTextField text] forKey:@"keyName"];
        
        if ([self.managedObjectContext save:&error]) {
            // Pop View Controller
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
        }
        
        if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
            // back button was pressed.  We know this is true because self is no longer
            // in the navigation stack.
            
        }
    }

    [super viewWillDisappear:animated];
}

- (NSArray *)loadKeyList:(NSString *)key {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyID == %@", key];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    return [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
}

#pragma mark - Alert

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Yes"]) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"masterKeyID == %@", [self.record valueForKey:@"keyID"]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *deleteArray = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        
        if (deleteArray != nil) {
            for (NSManagedObject *object in deleteArray) {
                [self.managedObjectContext deleteObject:object];
            }
        }
        
        [self.managedObjectContext deleteObject:self.record];
        [self.managedObjectContext save:&error];
        self.keySelected = @"";
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark - Action

- (IBAction)removeKey:(id)sender {
    NSLog(@"removeLock");
    [[[UIAlertView alloc] initWithTitle:@"Remove Key" message:@"Are you sure you want to remove this lock?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

@end
