//
//  NewMasterKeyTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "NewMasterKeyTableViewController.h"
#import "MasterKeyInfo.h"
#import "LockInfo.h"
#import "KeyList.h"

#define kKeyPickerIndex 4
#define kLockPickerIndex 2

#define kKeyPickerTag 99
#define kLockPickerTag 98

#define kPickerCellHeight 180

@interface NewMasterKeyTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (weak, nonatomic) IBOutlet UITextView *encryptKeyTextView;
@property (weak, nonatomic) IBOutlet UITextField *keyNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *keyNumberLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *keyNumberPicker;

@property (weak, nonatomic) IBOutlet UILabel *lockNumberLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *lockNumberPicker;

@property (assign) BOOL pickerIsShowing;
@property (assign) NSInteger selectPickerIndex;
@property (strong, nonatomic) LockInfo *selectLock;

@property (strong, nonatomic) NSMutableArray *keyAssignList;
@property (strong, nonatomic) NSArray *lockAssignList;

@end

@implementation NewMasterKeyTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"NewMasterKeyController -------------");
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lockID != %@", @"00000000-0000-0000-0000-000000000000"];
    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    self.lockAssignList = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    LockInfo *lockInfo = [self.lockAssignList objectAtIndex:0];
    self.selectLock = lockInfo;
    
    [self populateKeyAssignList:lockInfo];
    
    [self.keyNumberLabel setText:[self.keyAssignList objectAtIndex:0]];
    [self.lockNumberLabel setText:lockInfo.lockName];
    [self.lockNumberPicker setDelegate:self];
    [self.keyNumberPicker setDelegate:self];
    [self.keyNameTextField setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateKeyAssignList:(LockInfo *)lockInfo {
    
    [self.keyAssignList removeAllObjects];
    self.keyAssignList = [NSMutableArray arrayWithObjects:
                          @"Master Key #1", @"Master Key #2", @"Master Key #3", @"Master Key #4", @"Master Key #5",
                          @"Master Key #6", @"Master Key #7", @"Master Key #8", @"Master Key #9", @"Master Key #10",
                          @"Master Key #11", @"Master Key #12", @"Master Key #13", @"Master Key #14", @"Master Key #15",
                          @"Master Key #16", @"Master Key #17", @"Master Key #18", @"Master Key #19", @"Master Key #20",
                          nil];
    
    [[lockInfo masterKeys] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        MasterKeyInfo *keyInfo = obj;
        
        NSInteger removeKeyIndex;
        for (NSInteger i = 0; i < [self.keyAssignList count]; i++) {
            if ([keyInfo.keyNumber isEqualToString:[self.keyAssignList objectAtIndex:i]]) {
                removeKeyIndex = i;
                break;
            }
        }
        [self.keyAssignList removeObjectAtIndex:removeKeyIndex];
    }];
    
    [self.keyNumberPicker reloadAllComponents];
}

- (NSInteger)getKeyListIndex {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    return [fetchedObjects count];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.row == kKeyPickerIndex || indexPath.row == kLockPickerIndex) {
        if (self.selectPickerIndex == indexPath.row) {
            height = self.pickerIsShowing ? kPickerCellHeight : 0.0f;
        } else {
            height = 0.0f;
        }
    }
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        height = 100.0f;
    }
    
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1 || indexPath.row == 3){
        self.selectPickerIndex = indexPath.row+1;
        
        if (self.pickerIsShowing){
            
            [self hidePickerCell];
            
        }else {
            
            //            [self.activeTextField resignFirstResponder];
            
            [self showPickerCell];
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showPickerCell {
    
    self.pickerIsShowing = YES;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    if (self.selectPickerIndex == kKeyPickerIndex) {
        self.keyNumberPicker.hidden = NO;
        self.keyNumberPicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.keyNumberPicker.alpha = 1.0f;
        }];
    } else if (self.selectPickerIndex == kLockPickerIndex) {
        self.lockNumberPicker.hidden = NO;
        self.lockNumberPicker.alpha = 0.0f;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.lockNumberPicker.alpha = 1.0f;
        }];
    }
    
}

- (void)hidePickerCell {
    
    self.pickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.keyNumberPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.keyNumberPicker.hidden = YES;
                     }];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.lockNumberPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.lockNumberPicker.hidden = YES;
                     }];
    
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag == kLockPickerTag) {
//        [self.lockAssignList count];
        return [self.lockAssignList count];
    }
    
    if (pickerView.tag == kKeyPickerTag) {
        return [self.keyAssignList count];
    }
    
    return 0;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == kLockPickerTag) {
        
        LockInfo *lockInfo = [self.lockAssignList objectAtIndex:row];
        
        return lockInfo.lockName;
    }
    
    if (pickerView.tag == kKeyPickerTag) {
        
        return [self.keyAssignList objectAtIndex:row];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (self.selectPickerIndex == kKeyPickerIndex) {
        
        [self.keyNumberLabel setText:[self.keyAssignList objectAtIndex:row]];
        
    }
    
    if (self.selectPickerIndex == kLockPickerIndex) {
        
        LockInfo *lockInfo = [self.lockAssignList objectAtIndex:row];
        
        self.selectLock = lockInfo;
        
        [self.lockNumberLabel setText:lockInfo.lockName];
        
        [self populateKeyAssignList:lockInfo];
        
        [self.keyNumberLabel setText:[self.keyAssignList objectAtIndex:0]];
        [self.keyNumberPicker reloadAllComponents];
    }
}

#pragma mark - Action

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButton:(id)sender {
    MasterKeyInfo *saveKeyInfo = [NSEntityDescription insertNewObjectForEntityForName:@"MasterKeyInfo" inManagedObjectContext:[self managedObjectContext]];
    
    NSUUID *uuid = [[NSUUID alloc] init];
    saveKeyInfo.keyID = [uuid UUIDString];
    saveKeyInfo.encryptKey = [self.encryptKeyTextView text];
    
    NSString *keyName = [self.keyNameTextField text];
    if (keyName && [keyName length]) {
        saveKeyInfo.keyName = keyName;
    } else {
        keyName = [NSString stringWithFormat:@"%@ - G Key #%@", [self.selectLock lockName], [[self.keyNumberLabel text] substringFromIndex:11]];
        saveKeyInfo.keyName = keyName;
    }
    
    saveKeyInfo.keyNumber = [self.keyNumberLabel text];;
    saveKeyInfo.keyAssigned = [NSNumber numberWithBool:NO];
    saveKeyInfo.lockID = [self.selectLock lockID];
    saveKeyInfo.lockName = [self.selectLock lockName];
    
    [self.selectLock addMasterKeysObject:saveKeyInfo];
    
    KeyList *keyList = [NSEntityDescription insertNewObjectForEntityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    
    keyList.keyID = [uuid UUIDString];
    keyList.keyIndex = [NSNumber numberWithInteger:[self getKeyListIndex]];
    keyList.keyName = keyName;
    keyList.keyType = @"M";
    keyList.lockName = [self.selectLock lockName];
    keyList.lockID = saveKeyInfo.lockID;
    keyList.masterKeyID = saveKeyInfo.keyID;
    
    NSError *error;
    if ([[self managedObjectContext] save:&error]) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your new master key could not be saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
