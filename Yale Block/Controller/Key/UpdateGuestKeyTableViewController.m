//
//  UpdateGuestKeyTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "UpdateGuestKeyTableViewController.h"
#import "GuestKeyInfo.h"
#import "MasterKeyInfo.h"
#import "LockInfo.h"

@interface UpdateGuestKeyTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *removeButton;
@property (weak, nonatomic) IBOutlet UITextView *encryptKeyTextView;
@property (weak, nonatomic) IBOutlet UITextField *keyNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *masterKeyPickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *lockPickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *keyNumberPickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimePickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimePickerLabel;

@property (strong, nonatomic) GuestKeyInfo *guestKeyInfo;

@end

@implementation UpdateGuestKeyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"GuestKeyInfo" inManagedObjectContext:[self managedObjectContext]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyID == %@", self.keySelected];
    
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyNumber" ascending:YES]]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    self.guestKeyInfo = [fetchedObjects objectAtIndex:0];
    
    [self.encryptKeyTextView setEditable:NO];
    [self.encryptKeyTextView setText:[self.guestKeyInfo encryptKey]];
    
    [self.keyNameTextField setEnabled:NO];
    [self.keyNameTextField setText:[self.guestKeyInfo keyName]];
    
    MasterKeyInfo *masterKeyInfo = [self.guestKeyInfo masterKey];
    [self.masterKeyPickerLabel setText:masterKeyInfo.keyName];
    
    NSLog(@"masterKeyInfo lock: %@", [masterKeyInfo lock]);
    LockInfo *lockInfo = [masterKeyInfo lock];
    [self.lockPickerLabel setText:lockInfo.lockName];
    [self.keyNumberPickerLabel setText:[self.guestKeyInfo keyNumber]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    [self.startTimePickerLabel setText:[formatter stringFromDate:[self.guestKeyInfo startTime]]];
    [self.endTimePickerLabel setText:[formatter stringFromDate:[self.guestKeyInfo endTime]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Alert

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Yes"]) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyID == %@", [self.guestKeyInfo keyID]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *deleteArray = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        
        if (deleteArray != nil) {
            for (NSManagedObject *object in deleteArray) {
                [self.managedObjectContext deleteObject:object];
            }
        }
        [self.managedObjectContext save:&error];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

#pragma mark - Action

- (IBAction)removeKey:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Remove Guest Key" message:@"Are you sure you want to remove this key?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

@end
