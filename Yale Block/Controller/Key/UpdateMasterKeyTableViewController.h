//
//  UpdateMasterKeyTableViewController.h
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface UpdateMasterKeyTableViewController : UITableViewController  <UITextFieldDelegate>

@property (strong, nonatomic) NSString *keySelected;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
