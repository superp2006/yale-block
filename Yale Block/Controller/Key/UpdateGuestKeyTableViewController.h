//
//  UpdateGuestKeyTableViewController.h
//  Yale Block
//
//  Created by Peter on 8/10/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateGuestKeyTableViewController : UITableViewController

@property (strong, nonatomic) NSString *keySelected;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
