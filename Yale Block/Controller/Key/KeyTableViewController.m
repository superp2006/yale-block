//
//  KeyTableViewController.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "KeyTableViewController.h"
#import "NewMasterKeyTableViewController.h"
#import "UpdateMasterKeyTableViewController.h"
#import "NewGuestKeyTableViewController.h"
#import "UpdateGuestKeyTableViewController.h"
#import "CustomKeyTableViewCell.h"
#import "KeyList.h"
#import "MasterKeyInfo.h"

#define MASTER_KEY_SECTION 0
#define GUEST_KEY_SECTION 1
#define NEW_MASTER_KEY 0
#define UPDATE_MASTER_KEY 1
#define NEW_GUEST_KEY 0
#define UPDATE_GUEST_KEY 1

@interface KeyTableViewController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) IBOutlet UITableView *keysTableView;
@property (strong, nonatomic) NSIndexPath *cellSelected;
@property (strong, nonatomic) NSString *selectKey;

@end

@implementation KeyTableViewController

@synthesize managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"KeyTableViewController");
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"KeyList"];
    
    NSSortDescriptor *sortKeyIndex = [NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES];
    NSSortDescriptor *sortKeyType = [[NSSortDescriptor alloc] initWithKey:@"keyType" ascending:NO];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sortKeyType, sortKeyIndex, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"keyType" cacheName:nil];
    
    [self.fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
//    [[self.fetchedResultsController fetchedObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        KeyList *keyList = obj;
//        NSLog(@"keyList Name: %@", keyList.keyName);
//        NSLog(@"keyList ID: %@", keyList.keyID);
//        NSLog(@"keyList Type: %@", keyList.keyType);
//        NSLog(@"keyList index: %@", keyList.keyIndex);
//    }];
    
//    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: NO];
//    sectionTitleList = [[keyList allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortOrder, nil]];
//    NSLog(@"sectionTitleList: %@", sectionTitleList);
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fetched Results Controller Delegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    NSLog(@"controllerWillChangeContent");
    [self.keysTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            NSLog(@"NSFetchedResultChangeInsert");
            [self.keysTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            NSLog(@"NSFetchedResultsChangeDelete");
            [self.keysTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            NSLog(@"NSFetchedResultsChangeMove");
            break;
            
        case NSFetchedResultsChangeUpdate:
            NSLog(@"NSFetchedResultsChangeUpdate");
            break;
            
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch (type) {
        case NSFetchedResultsChangeInsert: {
//            NSLog(@"NSFetchedResultChangeInsert");
//            
//            NSLog(@"newIndexPath section: %zd", newIndexPath.section);
//            NSLog(@"newIndexPath row: %zd", newIndexPath.row);
//            
//            NSLog(@"newIndexPath length: %zd", [newIndexPath length]);
            
            [self.keysTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
            
        case NSFetchedResultsChangeDelete: {
//            NSLog(@"NSFetchedResultChangeDelete");
//            NSLog(@"indexPath row: %zd", indexPath.row);
            
            [self.keysTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
        case NSFetchedResultsChangeUpdate: {
//            NSLog(@"NSFetchedResultsChangeUpdate");
            [self configureCell:(CustomKeyTableViewCell *)[self.keysTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
            
        case NSFetchedResultsChangeMove: {
//            NSLog(@"NSFetchedResultsChangeMove");
            [self.keysTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.keysTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    NSLog(@"controllerDidChangeContent");
    [self.keysTableView endUpdates];
//    [self.keysTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    NSLog(@"numberOfSectionsInTableView");
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
//    NSLog(@"numberOfRowsInSection section: %zd", section);
//    NSLog(@"numberOfRowsInSection: %zd", [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    NSLog(@"titleForHeaderInSection");
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    if ([[sectionInfo indexTitle] isEqualToString:@"M"]) {
        return @"Master Key";
    }
    else {
        return @"Guest Key";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cellForRowAtIndexPath");
    CustomKeyTableViewCell *cell = (CustomKeyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(CustomKeyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        [cell setupView:@"addButton"];
    } else {
        [cell setupView:@"detailButton"];
    }
    
    [[cell keyLabel] setText:[record valueForKey:@"keyName"]];
    self.selectKey = [record valueForKey:@"keyID"];
    
    [cell setDidTapButtonBlock:^{
        self.cellSelected = indexPath;
        switch (indexPath.section) {
            case MASTER_KEY_SECTION: {
                if (indexPath.row == NEW_MASTER_KEY) {
                    [self performSegueWithIdentifier:@"newMasterKeyController" sender:self];
                } else {
                    [self performSegueWithIdentifier:@"updateMasterKeyController" sender:self];
                }
                break;
            }
                
            case GUEST_KEY_SECTION: {
                if (indexPath.row == NEW_GUEST_KEY) {
                    [self performSegueWithIdentifier:@"newGuestKeyController" sender:self];
                } else {
                    [self performSegueWithIdentifier:@"updateGuestKeyController" sender:self];
                }
                break;
            }
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath");
    self.cellSelected = indexPath;
    switch (indexPath.section) {
        case MASTER_KEY_SECTION: {
            if (indexPath.row == NEW_MASTER_KEY) {
                [self performSegueWithIdentifier:@"newMasterKeyController" sender:self];
            } else {
                [self performSegueWithIdentifier:@"updateMasterKeyController" sender:self];
            }
            break;
        }
            
        case GUEST_KEY_SECTION: {
            if (indexPath.row == NEW_GUEST_KEY) {
                [self performSegueWithIdentifier:@"newGuestKeyController" sender:self];
            } else {
                [self performSegueWithIdentifier:@"updateGuestKeyController" sender:self];
            }
            break;
        }
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"newMasterKeyController"]) {
        NSLog(@"prepareForSegue - newMasterKeyController");
        
        UINavigationController *newMasterKeyNavigationController = (UINavigationController *)[segue destinationViewController];
        
        NewMasterKeyTableViewController *newMasterKeyController = (NewMasterKeyTableViewController *) [newMasterKeyNavigationController topViewController];
        
        [newMasterKeyController setManagedObjectContext:[self managedObjectContext]];
        
    } else if ([segue.identifier isEqualToString:@"updateMasterKeyController"]) {
        
        NSLog(@"prepareForSegue - updateMasterKeyController");
        
        UpdateMasterKeyTableViewController *updateMasterKeyController = (UpdateMasterKeyTableViewController *) [segue destinationViewController];
        
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.cellSelected];
        [updateMasterKeyController setKeySelected:[record valueForKey:@"keyID"]];
        [updateMasterKeyController setManagedObjectContext:[self managedObjectContext]];
//        [updateMasterKeyController setRecord:record];
        
    } else if ([segue.identifier isEqualToString:@"newGuestKeyController"]) {
        NSLog(@"prepareForSegue - newGuestKeyController");
        
        UINavigationController *newGuestKeyNavigationController = (UINavigationController *)[segue destinationViewController];
        
        NewGuestKeyTableViewController *newGuestKeyController = (NewGuestKeyTableViewController *)[newGuestKeyNavigationController topViewController];
        
        [newGuestKeyController setManagedObjectContext:[self managedObjectContext]];
        
    } else if ([segue.identifier isEqualToString:@"updateGuestKeyController"]) {
        NSLog(@"prepareForSegue - updateGuestKeyController");
        
        UpdateGuestKeyTableViewController *updateGuestKeyController = (UpdateGuestKeyTableViewController *)[segue destinationViewController];
        
        [updateGuestKeyController setManagedObjectContext:[self managedObjectContext]];
        [updateGuestKeyController setKeySelected:self.selectKey];
    }
    
}

#pragma mark - Action


@end
