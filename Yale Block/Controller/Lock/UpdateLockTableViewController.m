//
//  UpdateLockTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/6/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "UpdateLockTableViewController.h"
#import "MasterKeyInfo.h"
#import "GuestKeyInfo.h"
#import "KeyList.h"

#define kTimePickerIndex 2
#define kTimePickerTag 96
#define kPickerCellHeight 180

#define singleComponent 2
#define tenthComponent 1
#define hundredthComponent 0

@interface UpdateLockTableViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (weak, nonatomic) IBOutlet UISwitch *lockMuteSwitch;
@property (weak, nonatomic) IBOutlet UITextView *uniqueIDTextView;
@property (weak, nonatomic) IBOutlet UITextField *lockNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *lockNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *autoLockLabel;

@property (assign) BOOL pickerIsShowing;
@property (strong, nonatomic) NSArray *singleDigits;
@property (strong, nonatomic) NSArray *tenthDigits;
@property (strong, nonatomic) NSArray *hundredthDigits;
@property (strong, nonatomic) LockInfo *lockInfo;

@property (assign) NSInteger autoLockSeconds;
@property (assign) NSInteger singleDigit;
@property (assign) NSInteger tenthDigit;
@property (assign) NSInteger hundredthDigit;

@end

@implementation UpdateLockTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"UpdateLockTableViewController -------------");

    NSLog(@"selectLockUUID: %@", self.selectLockUUID);
//    NSArray *recordList = [self loadDataWithEntity:self.selectLockUUID];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lockUUID == %@", self.selectLockUUID];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *recordList = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    self.lockInfo = [recordList objectAtIndex:0];
    
    self.singleDigits = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",nil];
    self.tenthDigits = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    self.hundredthDigits = [[NSArray alloc] initWithObjects:@"0",@"1", nil];
    
    self.autoLockSeconds = (NSInteger)[self.lockInfo autoLockTime];

    [self.lockNameTextField setText:[self.lockInfo lockName]];
    [self.lockMuteSwitch setOn:[[self.lockInfo mute] boolValue]];
    [self.uniqueIDTextView setText:[self.lockInfo lockEncryptKey]];
    [self.lockNumberLabel setText:[[self.lockInfo lockNumber] stringValue]];
    [self.autoLockLabel setText:[[self.lockInfo autoLockTime] stringValue]];
    
    NSInteger firstNumber = ABS((self.autoLockSeconds % 10));
    NSInteger secondNumber = ABS((self.autoLockSeconds % 100) / 10);
    NSInteger thirdNumber = ABS(self.autoLockSeconds / 100);
    
    self.singleDigit = firstNumber;
    self.tenthDigit = secondNumber * 10;
    self.hundredthDigit = thirdNumber * 100;
    
    [self.timePicker selectRow:firstNumber inComponent:singleComponent animated:NO];
    [self.timePicker selectRow:secondNumber inComponent:tenthComponent animated:NO];
    [self.timePicker selectRow:thirdNumber inComponent:hundredthComponent animated:NO];
    
    [self.lockNameTextField setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"viewWillDisappear");
    
    if ([self.selectLockUUID length] > 0) {
        [self.lockInfo setValue:[self.lockNameTextField text] forKey:@"lockName"];
        [self.lockInfo setValue:[self.uniqueIDTextView text] forKey:@"lockEncryptKey"];
        [self.lockInfo setValue:[NSNumber numberWithInt:(int)self.autoLockSeconds] forKey:@"autoLockTime"];
        [self.lockInfo setValue:[NSNumber numberWithBool:self.lockMuteSwitch.on] forKey:@"mute"];
        
        NSError *error = nil;
        
        if ([self.managedObjectContext save:&error]) {
            // Pop View Controller
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
        }
        
        if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
            // back button was pressed.  We know this is true because self is no longer
            // in the navigation stack.
            
        }
    }
    
    [super viewWillDisappear:animated];
}

- (NSArray *)loadDataWithEntity:(NSString *)lockID {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lockID == %@", lockID];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

#pragma mark - Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.row == kTimePickerIndex) {
        height = self.pickerIsShowing ? kPickerCellHeight : 0.0f;
    }
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        height = 100.0f;
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        
        if (self.pickerIsShowing){
            
            [self hidePickerCell];
            
        } else {
            
            [self showPickerCell];
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showPickerCell {
    
    self.pickerIsShowing = YES;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.timePicker.hidden = NO;
    self.timePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.timePicker.alpha = 1.0f;
    }];
    
    
}

- (void)hidePickerCell {
    
    self.pickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.timePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.timePicker.hidden = YES;
                     }];
    
}

#pragma mark - Picker view methods
- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    switch (component) {
        case singleComponent:
            return [self.singleDigits count];
            break;
            
        case tenthComponent:
            return [self.tenthDigits count];
            break;
            
        case hundredthComponent:
            return [self.hundredthDigits count];
            break;
    }
    
    return 0;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch (component) {
        case singleComponent:
            return [self.singleDigits objectAtIndex:row];
            break;
            
        case tenthComponent:
            return [self.tenthDigits objectAtIndex:row];
            break;
            
        case hundredthComponent:
            return [self.hundredthDigits objectAtIndex:row];
            break;
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"pickerView | singleDigit: %@", [NSString stringWithFormat:@"%zd", self.singleDigit]);
    NSLog(@"pickerView | tenthDigit: %@", [NSString stringWithFormat:@"%zd", self.tenthDigit]);
    NSLog(@"pickerView | hundredDigit: %@", [NSString stringWithFormat:@"%zd", self.hundredthDigit]);
    
    switch (component) {
        case singleComponent:
            self.singleDigit = row * 1;
            break;
            
        case tenthComponent:
            self.tenthDigit = row * 10;
            break;
            
        case hundredthComponent:
            self.hundredthDigit = row * 100;
            break;
    }
    
    self.autoLockSeconds = 0;
    self.autoLockSeconds += (self.singleDigit + self.tenthDigit + self.hundredthDigit);
    
    if (self.autoLockSeconds > 0) {
        [self.autoLockLabel setText:[NSString stringWithFormat:@"%zd", self.autoLockSeconds]];
    } else {
        [self.autoLockLabel setText:@"Off"];
    }
    
    
}

#pragma mark - Alert

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
     NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Yes"]) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lockID == %@", [self.lockInfo lockID]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *deleteArray = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        
        NSFetchRequest *keyListFetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *keyListEntity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
        
        NSPredicate *keyListPredicate = [NSPredicate predicateWithFormat:@"lockID == %@", [self.lockInfo lockID]];
        [keyListFetchRequest setPredicate:keyListPredicate];
        [keyListFetchRequest setEntity:keyListEntity];
        
        NSArray *deleteKeyListArray = [[self managedObjectContext] executeFetchRequest:keyListFetchRequest error:&error];
        
        NSFetchRequest *beaconFetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *beaconEntity = [NSEntityDescription entityForName:@"Beacons" inManagedObjectContext:[self managedObjectContext]];
        
        NSPredicate *beaconPredicate = [NSPredicate predicateWithFormat:@"(uuid = %@) AND (major = %d) AND (minor = %d)",self.selectBeaconUUID, self.selectBeaconMajorID, self.selectBeaconMinorID];
        
        [beaconFetchRequest setPredicate:beaconPredicate];
        [beaconFetchRequest setEntity:beaconEntity];
        
        NSArray *beaconDeleteArray = [[self managedObjectContext] executeFetchRequest:beaconFetchRequest error:&error];
        
        NSLog(@"deleteArray count: %zd", deleteArray.count);
        NSLog(@"beaconDeleteArray count: %zd", beaconDeleteArray.count);
        
        for (NSManagedObject *object in deleteArray) {
            [self.managedObjectContext deleteObject:object];
        }
        
        for (NSManagedObject *keyListObject in deleteKeyListArray) {
            [self.managedObjectContext deleteObject:keyListObject];
        }
        
        for (NSManagedObject *beaconObject in beaconDeleteArray) {
            [self.managedObjectContext deleteObject:beaconObject];
        }
        
//        [self.managedObjectContext deleteObject:self.lockInfo];
        [self.managedObjectContext save:&error];
        self.selectLockUUID = @"";
        self.selectBeaconMinorID = 0;
        self.selectBeaconMinorID = 0;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Action

- (IBAction)lockMute:(id)sender {
//    [self.record setValue:[NSNumber numberWithBool:self.lockMuteSwitch.on] forKey:@"mute"];
//    NSLog(@"lockMute: %@", [self.record valueForKey:@"mute"]);
}

- (IBAction)removeLock:(id)sender {
    NSLog(@"removeLock");
    [[[UIAlertView alloc] initWithTitle:@"Remove Lock" message:@"Are you sure you want to remove this lock?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
