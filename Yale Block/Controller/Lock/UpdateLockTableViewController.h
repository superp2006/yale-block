//
//  UpdateLockTableViewController.h
//  Yale Block
//
//  Created by Peter on 8/6/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LockInfo.h"

@interface UpdateLockTableViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) NSString *selectLockUUID;
@property (nonatomic,strong) NSString *selectBeaconUUID;
@property (assign) NSInteger selectBeaconMajorID;
@property (assign) NSInteger selectBeaconMinorID;

@property (assign) NSInteger lockCount;
@property (assign) NSInteger keyCount;

@end
