//
//  AddLockTableViewController.m
//  Yale Block
//
//  Created by Peter on 8/4/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "AddLockTableViewController.h"
#import "KeyList.h"
#import "MasterKeyInfo.h"
#import "LockInfo.h"
#import "Beacons.h"

#define kTimePickerIndex 2
#define kTimePickerTag 96
#define kPickerCellHeight 180

#define singleComponent 2
#define tenthComponent 1
#define hundredthComponent 0

@interface AddLockTableViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UISwitch *lockMuteSwitch;
@property (weak, nonatomic) IBOutlet UITextView *encryptKeyTextView;
@property (weak, nonatomic) IBOutlet UITextField *lockNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *lockNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *autoLockLabel;

@property (assign) BOOL pickerIsShowing;
@property (strong, nonatomic) NSArray *singleDigits;
@property (strong, nonatomic) NSArray *tenthDigits;
@property (strong, nonatomic) NSArray *hundredthDigits;
@property (assign) NSInteger autoLockSeconds;
@property (assign) NSInteger singleDigit;
@property (assign) NSInteger tenthDigit;
@property (assign) NSInteger hundredthDigit;
@property (assign) NSInteger keyListIndex;

@end

@implementation AddLockTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"AddLockTableViewController --------------");
    
    NSLog(@"selectedPeripheral: %@", self.selectLockUUID);
    NSLog(@"selectLockID: %@", self.selectLockID);
    
    self.singleDigits = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",nil];
    self.tenthDigits = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    self.hundredthDigits = [[NSArray alloc] initWithObjects:@"0",@"1", nil];
    self.autoLockSeconds = 0;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"keyIndex" ascending:YES]]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    self.keyListIndex = [fetchedObjects count];
    [self.lockNumberLabel setText:[NSString stringWithFormat:@"%zd", self.lockCount]];
    [self.lockNameTextField setDelegate:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.row == kTimePickerIndex) {
        height = self.pickerIsShowing ? kPickerCellHeight : 0.0f;
    }
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        height = 100.0f;
    }
    
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        
        if (self.pickerIsShowing) {
            
            [self hidePickerCell];
            
        } else {
            
            [self showPickerCell];
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showPickerCell {
    
    self.pickerIsShowing = YES;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.timePicker.hidden = NO;
    self.timePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.timePicker.alpha = 1.0f;
    }];
    
    
}

- (void)hidePickerCell {
    
    self.pickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.timePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.timePicker.hidden = YES;
                     }];
    
}

#pragma mark - Picker view methods
- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    switch (component) {
        case singleComponent:
            return [self.singleDigits count];
            break;
            
        case tenthComponent:
            return [self.tenthDigits count];
            break;
            
        case hundredthComponent:
            return [self.hundredthDigits count];
            break;
    }
    
    return 0;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch (component) {
        case singleComponent:
            return [self.singleDigits objectAtIndex:row];
            break;
            
        case tenthComponent:
            return [self.tenthDigits objectAtIndex:row];
            break;
            
        case hundredthComponent:
            return [self.hundredthDigits objectAtIndex:row];
            break;
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case singleComponent:
            self.singleDigit = row * 1;
            break;
            
        case tenthComponent:
            self.tenthDigit = row * 10;
            break;
            
        case hundredthComponent:
            self.hundredthDigit = row * 100;
            break;
    }
    
    self.autoLockSeconds = 0;
    self.autoLockSeconds += (self.singleDigit + self.tenthDigit + self.hundredthDigit);
    
    if (self.autoLockSeconds > 0) {
        [self.autoLockLabel setText:[NSString stringWithFormat:@"%zd", self.autoLockSeconds]];
    } else {
        [self.autoLockLabel setText:@"Off"];
    }

}

#pragma mark - Action


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)save:(id)sender {
    
    NSUUID *keyUuid = [[NSUUID alloc] init];
    
    LockInfo *saveLockInfo = [NSEntityDescription insertNewObjectForEntityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    saveLockInfo.lockNumber = [NSNumber numberWithInt:(int)[self lockCount]];
    saveLockInfo.lockID = self.selectLockID;
    saveLockInfo.lockUUID = self.selectLockUUID;
    NSString *lockName = [self.lockNameTextField text];
    if (lockName && [lockName length]) {
        saveLockInfo.lockName = lockName;
    } else {
        
        saveLockInfo.lockName = [NSString stringWithFormat:@"Lock #%zd", [self lockCount]];
    }
    saveLockInfo.autoLockTime = [NSNumber numberWithInt:(int)self.autoLockSeconds];
    saveLockInfo.lockEncryptKey = [self.encryptKeyTextView text];
    saveLockInfo.mute = [NSNumber numberWithBool:[self.lockMuteSwitch isOn]];        
    
    MasterKeyInfo *masterKeyInfo = [NSEntityDescription insertNewObjectForEntityForName:@"MasterKeyInfo" inManagedObjectContext:[self managedObjectContext]];
    
    masterKeyInfo.keyID = [keyUuid UUIDString];
    masterKeyInfo.keyName = [saveLockInfo.lockName stringByAppendingString:@" - M Key #1"];
    masterKeyInfo.keyNumber = @"Master Key #1";
    masterKeyInfo.keyAssigned = [NSNumber numberWithBool:YES];
    masterKeyInfo.encryptKey = @"None";
    masterKeyInfo.lockID = self.selectLockID;
    masterKeyInfo.lockName = saveLockInfo.lockName;
    
    [saveLockInfo addMasterKeysObject:masterKeyInfo];
    
    KeyList *keyList = [NSEntityDescription insertNewObjectForEntityForName:@"KeyList" inManagedObjectContext:[self managedObjectContext]];
    
    keyList.keyID = [keyUuid UUIDString];
    keyList.keyIndex = [NSNumber numberWithInteger:self.keyListIndex];
    keyList.keyName = masterKeyInfo.keyName;
    keyList.keyType = @"M";
    keyList.lockName = [self.lockNameTextField text];
    keyList.lockID = self.selectLockID;
    keyList.masterKeyID = [keyUuid UUIDString];
    
    Beacons *beacon = [NSEntityDescription insertNewObjectForEntityForName:@"Beacons" inManagedObjectContext:[self managedObjectContext]];
    
    beacon.name = self.selectLockID;
    beacon.uuid = self.selectBeaconUUID;
    beacon.major = [NSNumber numberWithInteger:self.selectBeaconMajorID];
    beacon.minor = [NSNumber numberWithInteger:self.selectBeaconMinorID];
    beacon.event = @"EventNear";
    beacon.enable = [NSNumber numberWithBool:YES];
    beacon.action = @"Play Alarm";
    
    NSError *error;
    if ([[self managedObjectContext] save:&error]) {

        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        if (error) {
            NSLog(@"Unable to save record.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your new lock could not be saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (IBAction)lockMute:(id)sender {
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end