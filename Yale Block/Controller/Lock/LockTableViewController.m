//
//  LockTableViewController.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "LockTableViewController.h"
#import "AddLockTableViewController.h"
#import "UpdateLockTableViewController.h"
#import "CustomLockTableViewCell.h"
#import "LockInfo.h"
#import "BleDevice.h"
#import "BeaconDevice.h"
#import "LockData.h"

@interface LockTableViewController () <NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *reScanLock;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSIndexPath *cellSeleced;
@property (strong, nonatomic) NSString *selectedPeripheralUUID;
@property (strong, nonatomic) NSString *selectedPeripheralID;
@property (strong, nonatomic) NSString *selectedBeaconUUID;
@property (assign) NSInteger selectedMajorID;
@property (assign) NSInteger selectedMinorID;
@property (assign) NSInteger lockCount;
@property (assign) NSInteger keyCount;

@end

@implementation LockTableViewController

@synthesize managedObjectContext;
@synthesize lockInfos;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"LockTableViewController --------------");
    
    [self setLockCount:0];
    [self setKeyCount:0];
//    [[LeDiscovery sharedInstance] setDiscoveryDelegate:self];
//    [[LeDiscovery sharedInstance] setPeripheralDelegate:self];
//    [[LeDiscovery sharedInstance] setIsLockTableLoaded:TRUE];
    
//    [[BeaconManage sharedInstance] setBeaconDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDevice) name:NSManagedObjectContextDidSaveNotification object:nil];
            
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
//    
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
//    [fetchRequest setEntity:entity];
//    
//    NSError *error;
//    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
//    
//    NSLog(@"%@",fetchedObjects);
    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LockInfo"];
//
//    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
//
//    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
//
//    [self.fetchedResultsController setDelegate:self];
//    
//    NSError *error = nil;
//    [self.fetchedResultsController performFetch:&error];
//    
//    if (error) {
//        NSLog(@"Unable to perform fetch.");
//        NSLog(@"%@, %@", error, error.localizedDescription);
//    }
    
    [self.lockTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillAppear");
    [[LeDiscovery sharedInstance] setDiscoveryDelegate:self];
    [[LeDiscovery sharedInstance] setPeripheralDelegate:self];
    [[BeaconManage sharedInstance] setIsInLockTable:TRUE];
    
    [[BeaconManage sharedInstance] setBeaconDelegate:self];
    [self.lockTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[BeaconManage sharedInstance] setIsInLockTable:FALSE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark LeDiscoveryDelegate

- (void) discoveryDidRefresh {
    NSLog(@"discoveryDidRefresh");
    [self.lockTableView reloadData];
}

#pragma mark -
#pragma mark BeaconManageDelegate

- (void) getBleDeviceData {
    NSLog(@"getBleDeviceData");
    [[BeaconManage sharedInstance] setBleDeviceList:[[LeDiscovery sharedInstance] bleDeviceList]];
    [[BeaconManage sharedInstance] loadLockData];
}

- (void) reloadLockDataTable {
    NSLog(@"reloadLockDataTable");
    [self.lockTableView reloadData];
}

#pragma mark -
#pragma mark Handle notification

- (void) reloadDevice {
    NSLog(@"reloadDevice -----");
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];

    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    NSFetchRequest *beaconFetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *beaconEntity = [NSEntityDescription entityForName:@"Beacons" inManagedObjectContext:[self managedObjectContext]];
    
    [beaconFetchRequest setEntity:beaconEntity];
    
    NSArray *fetchedBeaconObjects = [[self managedObjectContext] executeFetchRequest:beaconFetchRequest error:&error];
    
    [[LeDiscovery sharedInstance] loadSavedDevices:fetchedObjects];
    [[BeaconManage sharedInstance] loadSavedBeacons:fetchedBeaconObjects addBleDevices:[[LeDiscovery sharedInstance] bleDeviceList]];
}

#pragma mark -
#pragma mark TableView Delegates

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomLockTableViewCell *cell = (CustomLockTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(CustomLockTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *lockName;
    NSString *lockUUID;
    
    NSArray *record = [[BeaconManage sharedInstance] lockDataList];
//    NSArray *record = [[LeDiscovery sharedInstance] foundPeripherals];
    NSMutableArray *list = [[LeDiscovery sharedInstance] lockList];
    
    NSLog(@"configureCell lockListCount: %zd", list.count);
    
    LockData *lockData = record[indexPath.row];
    
//    CBPeripheral *peripheral= record[indexPath.row];
    NSString *peripheralName = lockData.bleID;
    NSString *peripheralUUID = lockData.bleUUID;
    NSString *beaconUUID = lockData.beaconUUID;
    NSInteger beaconMajorID = [lockData.beaconMajor integerValue];
    NSInteger beaconMinorID = [lockData.beaconMinor integerValue];
    
    NSLog(@"periperalName: %@", peripheralName);
        
    BOOL isAddTag = FALSE;
    
    for (LockInfo *lockInfo in list) {
        if ([peripheralUUID isEqualToString:lockInfo.lockUUID]) {
            lockName = lockInfo.lockName;
            lockUUID = lockInfo.lockUUID;
            break;
        }
    }
    
    if (!lockName.length) {
        lockUUID = peripheralUUID;
        lockName = peripheralName;
        
        isAddTag = TRUE;
        [cell setupView:@"addButton"];
    } else {
        [cell setupView:@"detailButton"];
    }
    
    [[cell lockTextLabel] setText:lockName];
    
    [cell setDidTapButtonBlock:^{
        self.cellSeleced = indexPath;
        self.selectedPeripheralUUID = lockUUID;
        self.selectedPeripheralID = lockData.bleID;
        self.selectedMajorID = beaconMajorID;
        self.selectedMinorID = beaconMinorID;
        self.selectedBeaconUUID = beaconUUID;
        if (isAddTag) {
            [self performSegueWithIdentifier:@"addLockViewController" sender:self];
        } else {
            [self performSegueWithIdentifier:@"updateLockViewController" sender:self];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    NSArray *allLockDatas = [[BeaconManage sharedInstance] lockDataList];
    if (allLockDatas.count <= 0) {
        //create a lable size to fit the Table View
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                                        self.tableView.bounds.size.width,
                                                                        self.tableView.bounds.size.height)];
        //set the message
        messageLbl.text = @"No saved lock found";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        //auto size the text
        [messageLbl sizeToFit];
        
        //set back to label view
        self.tableView.backgroundView = messageLbl;
        //no separator
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    } else {
        self.tableView.backgroundView = nil;
    }
    
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"numberOfRowsInSection: %zd", [[[LeDiscovery sharedInstance] foundPeripherals] count]);
//    return [[[LeDiscovery sharedInstance] foundPeripherals] count];
    NSLog(@"numberOfRowsInSection: %zd", [[[BeaconManage sharedInstance] lockDataList] count]);
    return [[[BeaconManage sharedInstance] lockDataList] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    self.cellSeleced = indexPath;
//    if (indexPath.row == 0) {
//        [self performSegueWithIdentifier:@"addLockViewController" sender:self];
//    } else {
//        [self performSegueWithIdentifier:@"updateLockViewController" sender:self];
//    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"addLockViewController"]) {
        NSLog(@"prepareForSegue - addLockViewController");

        UINavigationController *lockDetailNavigationController = (UINavigationController *)[segue destinationViewController];
        AddLockTableViewController *addLockViewController = (AddLockTableViewController *)[lockDetailNavigationController topViewController];

        [addLockViewController setManagedObjectContext:[self managedObjectContext]];
        [addLockViewController setLockCount:[self lockCount]];
        [addLockViewController setSelectLockUUID:self.selectedPeripheralUUID];
        [addLockViewController setSelectLockID:self.selectedPeripheralID];
        [addLockViewController setSelectBeaconUUID:self.selectedBeaconUUID];
        [addLockViewController setSelectBeaconMajorID:self.selectedMajorID];
        [addLockViewController setSelectBeaconMinorID:self.selectedMinorID];

    } else if ([segue.identifier isEqualToString:@"updateLockViewController"]){
        NSLog(@"prepareForSegue - updateLockViewController");

        UpdateLockTableViewController *updateLockViewController = (UpdateLockTableViewController *)[segue destinationViewController];

//        LockInfo *record = [self.fetchedResultsController objectAtIndexPath:self.cellSeleced];
        
//        NSArray *lockList = [[LeDiscovery sharedInstance] lockList];
//        
//        for (LockInfo *lockInfo in lockList) {
//            if ([lockInfo.lockID isEqualToString:self.selectedPeripheralID]) {
//                [updateLockViewController ]
//            }
//        }
        
        [updateLockViewController setManagedObjectContext:[self managedObjectContext]];
        [updateLockViewController setSelectLockUUID:self.selectedPeripheralUUID];
        [updateLockViewController setSelectBeaconMajorID:self.selectedMajorID];
        [updateLockViewController setSelectBeaconMinorID:self.selectedMinorID];
        [updateLockViewController setSelectBeaconUUID:self.selectedBeaconUUID];
    }

}

//
//#pragma mark - Fetched Results Controller Delegate Methods
//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
//    [self.lockTableView beginUpdates];
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
//    
//    switch (type) {
//        case NSFetchedResultsChangeInsert: {
//            [self.lockTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//        }
//            
//        case NSFetchedResultsChangeDelete: {
//            [self.lockTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//        }
//        
//        case NSFetchedResultsChangeUpdate: {
//            [self configureCell:(CustomLockTableViewCell *)[self.lockTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
//            break;
//        }
//            
//        case NSFetchedResultsChangeMove: {
//            [self.lockTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            [self.lockTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//        }
//    }
//}
//
//- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [self.lockTableView endUpdates];
//}
//
//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return [[self.fetchedResultsController sections] count];
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSArray *sections = [self.fetchedResultsController sections];
//    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
//    
//    [self setLockCount:[sectionInfo numberOfObjects]];
//    return [sectionInfo numberOfObjects];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    CustomLockTableViewCell *cell = (CustomLockTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//    [self configureCell:cell atIndexPath:indexPath];
//    
//    return cell;
//}
//
//- (void)configureCell:(CustomLockTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
//    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    if (indexPath.row == 0) {
//        [cell setupView:@"addButton"];
//    } else {
//        [cell setupView:@"detailButton"];
//    }
//    
//    [[cell lockTextLabel] setText:[record valueForKey:@"lockName"]];
//    
//    [cell setDidTapButtonBlock:^{
//        self.cellSeleced = indexPath;
//        if (indexPath.row == 0) {
//            [self performSegueWithIdentifier:@"addLockViewController" sender:self];
//        } else {
//            [self performSegueWithIdentifier:@"updateLockViewController" sender:self];
//        }
//    }];
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    self.cellSeleced = indexPath;
//    if (indexPath.row == 0) {
//        [self performSegueWithIdentifier:@"addLockViewController" sender:self];
//    } else {
//        [self performSegueWithIdentifier:@"updateLockViewController" sender:self];
//    }
//}
//
//#pragma mark - Navigation
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    if ([segue.identifier isEqualToString:@"addLockViewController"]) {
//        NSLog(@"prepareForSegue - addLockViewController");
//        
//        UINavigationController *lockDetailNavigationController = (UINavigationController *)[segue destinationViewController];
//        AddLockTableViewController *addLockViewController = (AddLockTableViewController *)[lockDetailNavigationController topViewController];
//        
//        [addLockViewController setManagedObjectContext:[self managedObjectContext]];
//        [addLockViewController setLockCount:[self lockCount]];
//
//    } else if ([segue.identifier isEqualToString:@"updateLockViewController"]){
//        NSLog(@"prepareForSegue - updateLockViewController");
//        
//        UpdateLockTableViewController *updateLockViewController = (UpdateLockTableViewController *)[segue destinationViewController];
//        
//        LockInfo *record = [self.fetchedResultsController objectAtIndexPath:self.cellSeleced];
//        
//        [updateLockViewController setManagedObjectContext:[self managedObjectContext]];
//        [updateLockViewController setSelectLockID:record.lockID];
//    }
//    
//}

@end
