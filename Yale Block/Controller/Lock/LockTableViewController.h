//
//  LockTableViewController.h
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <QuartzCore/QuartzCore.h>
#import "LeLockService.h"
#import "LeDiscovery.h"
#import "BeaconManage.h"

@interface LockTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, LeDiscoveryDelegate, LeLockServiceProtocol, BeaconManageDelegate>

@property (strong, nonatomic) IBOutlet UITableView *lockTableView;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,strong) NSArray *lockInfos;

@end
