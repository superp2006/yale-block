//
//  AppDelegate.m
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeTableViewController.h"
#import "KeyTableViewController.h"
#import "LockTableViewController.h"
#import "RecordTableViewController.h"
#import "SettingTableViewController.h"
#import "Beacons.h"

@interface AppDelegate () {
    
}
@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize motionManager;
@synthesize alarmPlayer;
@synthesize xData,yData,zData;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initSound];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    if( motionManager == nil) {
        motionManager=[[CMMotionManager alloc] init];
    }
    
    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        xData=accelerometerData.acceleration.x;
        yData=accelerometerData.acceleration.y;
        zData=accelerometerData.acceleration.z;
    }];    
    
    NSMutableArray *test = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4", nil];
    
    [test removeObjectAtIndex:0];
    
    NSLog(@"test: %@", [test objectAtIndex:0]);
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lockNumber" ascending:YES]]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LockInfo" inManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    NSFetchRequest *beaconFetchRequest = [[NSFetchRequest alloc] init];
    
    [beaconFetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    NSEntityDescription *beaonEntity = [NSEntityDescription entityForName:@"Beacons" inManagedObjectContext:[self managedObjectContext]];
    [beaconFetchRequest setEntity:beaonEntity];
    
    NSError *beaconError;
    NSArray *fetchedBeacons = [[self managedObjectContext] executeFetchRequest:beaconFetchRequest error:&beaconError];
    
    [[LeDiscovery sharedInstance] setDiscoveryDelegate:self];
    [[LeDiscovery sharedInstance] setPeripheralDelegate:self];
    [[LeDiscovery sharedInstance] loadSavedDevices:fetchedObjects];
    
    [[BeaconManage sharedInstance] setBeaconDelegate:self];
    [[BeaconManage sharedInstance] loadSavedBeacons:fetchedBeacons addBleDevices:[[LeDiscovery sharedInstance] bleDeviceList]];
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    UINavigationController *homeNavigationController = [[tabBarController viewControllers] objectAtIndex:0];
    HomeTableViewController *homeTableViewController = [[homeNavigationController viewControllers] objectAtIndex:0];
    homeTableViewController.managedObjectContext = self.managedObjectContext;
    
    UINavigationController *keyNavigationController = [[tabBarController viewControllers] objectAtIndex:1];
    KeyTableViewController *keyTableViewController = [[keyNavigationController viewControllers] objectAtIndex:0];
    keyTableViewController.managedObjectContext = self.managedObjectContext;
    
    UINavigationController *lockNavigationController = [[tabBarController viewControllers] objectAtIndex:2];
    LockTableViewController *lockTableViewController = [[lockNavigationController viewControllers] objectAtIndex:0];
    lockTableViewController.managedObjectContext = self.managedObjectContext;
    
    UINavigationController *recordNavigationController = [[tabBarController viewControllers] objectAtIndex:3];
    RecordTableViewController *recordTableViewController = [[recordNavigationController viewControllers] objectAtIndex:0];
    recordTableViewController.managedObjectContext = self.managedObjectContext;
    
    UINavigationController *settingNavigationController = [[tabBarController viewControllers] objectAtIndex:4];
    SettingTableViewController *settingTableViewController = [[settingNavigationController viewControllers] objectAtIndex:0];
    settingTableViewController.managedObjectContext = self.managedObjectContext;
    
    return YES;
}

#pragma mark - Sound

- (void) initSound {
    NSError *error  = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"sounds-1068-the-calling" ofType:@"mp3"]];
    alarmPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        [alarmPlayer prepareToPlay];
    }
}

- (void)playAlarm {
    NSLog(@"playAlarm");
    [alarmPlayer play];
    [self performSelector:@selector(stopAlarm) withObject:nil afterDelay:2];
}


- (void)stopAlarm {
    NSLog(@"stopAlarm");
    [alarmPlayer stop];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:TRUE] forKey:@"background"];

    NSNotification *notification = [[NSNotification alloc] initWithName:@"EnterBackground" object:nil userInfo:dictionary];
    [[NSNotificationCenter defaultCenter] postNotification:notification];    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:FALSE] forKey:@"background"];
    
    NSNotification *notification = [[NSNotification alloc] initWithName:@"EnterBackground" object:nil userInfo:dictionary];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
//    [motionManager stopAccelerometerUpdates];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"yaleblock" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"yaleblock.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
