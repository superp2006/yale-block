//
//  CustomLockTableViewCell.h
//  Yale Block
//
//  Created by Peter on 7/31/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CellDidTapButtonBlock)();

@interface CustomLockTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lockTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *lockButton;

@property (copy, nonatomic) CellDidTapButtonBlock didTapButtonBlock;

- (void)setupView:(NSString *)style;

@end
