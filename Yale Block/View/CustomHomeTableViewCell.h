//
//  CustomHomeTableViewCell.h
//  Yale Block
//
//  Created by Peter on 10/15/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CellDidTapLockButtonBlock)();
typedef void (^CellDidTapUnlockButtonBlock)();

@interface CustomHomeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *lockButton;
@property (weak, nonatomic) IBOutlet UIButton *unlockButton;
@property (weak, nonatomic) IBOutlet UILabel *lockNameLabel;

@property (copy, nonatomic) CellDidTapLockButtonBlock didTapLockButtonBlock;
@property (copy, nonatomic) CellDidTapUnlockButtonBlock didTapUnlockButtonBlock;
- (void)setupView:(NSString *)style;

@end
