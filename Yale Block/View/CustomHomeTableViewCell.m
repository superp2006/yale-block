//
//  CustomHomeTableViewCell.m
//  Yale Block
//
//  Created by Peter on 10/15/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "CustomHomeTableViewCell.h"
#import "IonIcons.h"

@implementation CustomHomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupView:(NSString *)style {
    //    NSLog(@"setupView style: %@", style);
    UIImage *lockIcon = [IonIcons imageWithIcon:ion_locked iconColor:[UIColor redColor]
                                       iconSize:80.0f
                                      imageSize:CGSizeMake(80.0f, 80.0f)];
    [self.lockButton setImage:lockIcon forState:UIControlStateNormal];
    
    UIImage *unlockIcon = [IonIcons imageWithIcon:ion_unlocked
                                      iconColor:[UIColor redColor]
                                       iconSize:80.0f
                                      imageSize:CGSizeMake(80.0f, 80.0f)];
    [self.unlockButton setImage:unlockIcon forState:UIControlStateNormal];
    
    [self.lockButton addTarget:self action:@selector(didTapLockButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.unlockButton addTarget:self action:@selector(didTapUnlockButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didTapLockButton:(UIButton *)button {
    if (self.didTapLockButtonBlock) {
        self.didTapLockButtonBlock();
    }
}

- (void)didTapUnlockButton:(UIButton *)button {
    if (self.didTapUnlockButtonBlock) {
        self.didTapUnlockButtonBlock();
    }
}


@end
