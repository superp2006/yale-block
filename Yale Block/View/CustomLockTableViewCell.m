//
//  CustomLockTableViewCell.m
//  Yale Block
//
//  Created by Peter on 7/31/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import "CustomLockTableViewCell.h"
#import "IonIcons.h"

@implementation CustomLockTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
//    NSLog(@"CustomLockTableViewCell ------------");
//    [self setupView:@"addButton"];
//    [self setupView:self.buttonStyle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupView:(NSString *)style {
//    NSLog(@"setupView style: %@", style);
    if ([style isEqualToString:@"addButton"]) {
        UIImage *icon = [IonIcons imageWithIcon:ion_plus iconColor:[UIColor redColor]
                                       iconSize:25.0f
                                      imageSize:CGSizeMake(25.0f, 25.0f)];
        [self.lockButton setImage:icon forState:UIControlStateNormal];
    } else {
        UIImage *icon = [IonIcons imageWithIcon:ion_chevron_right
                                      iconColor:[UIColor redColor]
                                       iconSize:25.0f
                                      imageSize:CGSizeMake(25.0f, 25.0f)];
        [self.lockButton setImage:icon forState:UIControlStateNormal];
    }
    [self.lockButton addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didTapButton:(UIButton *)button {
    if (self.didTapButtonBlock) {
        self.didTapButtonBlock();
    }
}

@end
