//
//  AppDelegate.h
//  Yale Block
//
//  Created by Peter on 7/15/15.
//  Copyright (c) 2015 ardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>
#import <AVFoundation/AVFoundation.h>
#import "LeDiscovery.h"
#import "LeLockService.h"
#import "BeaconManage.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, LeLockServiceProtocol, LeDiscoveryDelegate, BeaconManageDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) AVAudioPlayer *alarmPlayer;
@property (assign) double xData, yData, zData;

- (void)playAlarm;
- (void)stopAlarm;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

