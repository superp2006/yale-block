//
//  BeaconManage.m
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "BeaconManage.h"
#import "Beacons.h"
#import "BeaconDevice.h"
#import "BleDevice.h"
#import "LockData.h"
#import "LockInfo.h"

@interface BeaconManage() {
    CLLocationManager *locationManager;
}

@end

@implementation BeaconManage

@synthesize foundBeacons;
@synthesize beaconList;
@synthesize beaconDelegate;
@synthesize beaconDeviceList;
@synthesize bleDeviceList;
@synthesize lockDataList;
@synthesize isInLockTable;
@synthesize isInHome;

#pragma mark -
#pragma mark Init

+ (id) sharedInstance {
    static BeaconManage *this = nil;
    
    if (!this)
        this = [[BeaconManage alloc] init];
    
    return this;
}

- (id) init {
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestAlwaysAuthorization];
        }
        [locationManager setPausesLocationUpdatesAutomatically:NO];
        locationManager.delegate = self;
        foundBeacons = [[NSMutableArray alloc] init];
        beaconList = [[NSMutableArray alloc] init];
        beaconDeviceList = [[NSMutableArray alloc] init];
        bleDeviceList = [[NSMutableArray alloc] init];
        lockDataList = [[NSMutableArray alloc] init];
        isInLockTable = FALSE;
        isInHome = FALSE;
    }
    return self;
}

- (void) loadSavedBeacons:(NSArray *)beaconDevices addBleDevices:(NSArray *) bleDevices{
    NSLog(@"loadSavedBeacons");
    [lockDataList removeAllObjects];
    [beaconDeviceList removeAllObjects];
    
    if (![beaconDevices isKindOfClass:[NSArray class]]) {
        NSLog(@"No stored array to load");
        return;
    }
    
    if ([beaconDevices count] <= 0) {
        NSLog(@"No beacon data loaded");
        return;
    }
    
    if ([bleDevices count] <= 0) {
        NSLog(@"No ble data loaded");
        return;
    }
    
    bleDeviceList = [NSMutableArray arrayWithArray:bleDevices];
//    beaconList = [NSMutableArray arrayWithArray:beaconDevices];
    
    for (Beacons *beacon in beaconDevices) {
        for (BleDevice *ble in bleDeviceList) {
            if ([beacon.name isEqualToString:ble.deviceName]) {
                LockData *lockData = [[LockData alloc] init];
                lockData.bleID = ble.deviceName;
                lockData.bleUUID = ble.deviceUUID;
                lockData.beaconUUID = beacon.uuid;
                lockData.beaconMajor = beacon.major;
                lockData.beaconMinor = beacon.minor;
                lockData.inDatabase = [NSNumber numberWithBool:YES];
                
//                NSLog(@"LockData bleID: %@", lockData.bleID);
//                NSLog(@"LockData bleUUID: %@", lockData.bleUUID);
//                NSLog(@"LockData beaconUUID: %@", lockData.beaconUUID);
                [lockDataList addObject:lockData];
                break;
            }
        }
    }
    if (isInLockTable) {
        [beaconDelegate reloadLockDataTable];
    }
    
}

- (void) loadLockData {
    NSLog(@"loadLockData -----");
    NSLog(@"bleDeviceList=%zd beaconDeviceList=%zd", bleDeviceList.count, beaconDeviceList.count);
    BOOL reloadTableNow = FALSE;
    if (bleDeviceList.count == 0) {
        return;
    }
    
    if (beaconDeviceList.count == 0) {
        return;
    }
    
    for (BleDevice *ble in bleDeviceList) {
        BOOL isLockDataExist = FALSE;
        
        for (LockData *lockData in lockDataList) {
            if ([lockData.bleUUID isEqualToString:ble.deviceUUID]) {
                isLockDataExist = TRUE;
                break;
            }
        }
        
        if (!isLockDataExist) {
            for (BeaconDevice *beacon in beaconDeviceList) {
                long beaconRssi = [beacon.beaconRssi longValue];
                long bleRssi = [ble.deviceRssi longValue];

                if (bleRssi < -65) {
                    NSLog(@"Lock is not within range");
                    break;
                }

                long rssiA = (beaconRssi - 5);
                long rssiB = (beaconRssi + 5);

//                NSLog(@"beaconRssi - 5: %zd", rssiA);
//                NSLog(@"beaconRssi + 5: %zd", rssiB);
//                NSLog(@"bleRssi: %zd", bleRssi);

                if ((rssiA < bleRssi) && (rssiB > bleRssi)) {
                    LockData *lockData = [[LockData alloc] init];
                    lockData.bleID = ble.deviceName;
                    lockData.bleUUID = ble.deviceUUID;
                    lockData.beaconUUID = beacon.beaconUUID;
                    lockData.beaconMajor = beacon.beaconMajor;
                    lockData.beaconMinor = beacon.beaconMinor;
                    lockData.inDatabase = [NSNumber numberWithBool:NO];
//                    NSLog(@"LockData bleID: %@", lockData.bleID);
//                    NSLog(@"LockData bleUUID: %@", lockData.bleUUID);
//                    NSLog(@"LockData beaconUUID: %@", lockData.beaconUUID);
                    
                    [lockDataList addObject:lockData];
                    reloadTableNow = TRUE;
                    break;
                }
            }
        }
        
    }
    
    if (reloadTableNow) {
        if (isInLockTable) {
            [beaconDelegate reloadLockDataTable];
        }
    }
    
//    for (BeaconDevice *beacon in beaconDeviceList) {
//        for (BleDevice *ble in bleDeviceList) {
//            long beaconRssi = [beacon.beaconRssi longValue];
//            long bleRssi = [ble.deviceRssi longValue];
//            
//            if (bleRssi < -65) {
//                NSLog(@"Lock is not within range");
//                break;
//            }
//            
//            long rssiA = (beaconRssi - 5);
//            long rssiB = (beaconRssi + 5);
//            
//            NSLog(@"beaconRssi - 5: %zd", rssiA);
//            NSLog(@"beaconRssi + 5: %zd", rssiB);
//            NSLog(@"bleRssi: %zd", bleRssi);
//            
//            if ((rssiA < bleRssi) && (rssiB > bleRssi)) {
//                LockData *lockData = [[LockData alloc] init];
//                lockData.bleID = ble.deviceName;
//                lockData.bleUUID = ble.deviceUUID;
//                lockData.beaconUUID = beacon.beaconUUID;
//                lockData.beaconMajor = beacon.beaconMajor;
//                lockData.beaconMinor = beacon.beaconMinor;
//                NSLog(@"LockData bleID: %@", lockData.bleID);
//                NSLog(@"LockData bleUUID: %@", lockData.bleUUID);
//                NSLog(@"LockData beaconUUID: %@", lockData.beaconUUID);
//                
//                [lockDataList addObject:lockData];
//                break;
//            }
//        }
//    }
    
}


#pragma mark -
#pragma mark Monitor beacon

- (void) startMonitoringBeacon {
    NSLog(@"startMonitoringBeacon");
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"92213327-6C53-4BE5-BB00-A94B83F4C910"];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:[NSString stringWithFormat: @"SYBT-15209001"]];
    
    region.notifyEntryStateOnDisplay = YES;
    [locationManager startMonitoringForRegion:region];
    [locationManager startRangingBeaconsInRegion:region];
}

- (void) stopMonitoringBeacon {
    NSLog(@"stopMonitoringBeacon");
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"92213327-6C53-4BE5-BB00-A94B83F4C910"];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:[NSString stringWithFormat: @"SYBT-15209001"]];
    
    [locationManager stopMonitoringForRegion:region];
    [locationManager stopRangingBeaconsInRegion:region];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    NSLog(@"didDetermineState");
//    [self performActionForEvent:EventEnter withIdentifier:region.identifier];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Failed monitoring region: %@", error);
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
//    NSLog(@"didStartMonitoringForRegion");
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location manager failed: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *) region {
    
    BOOL loadLockDataNow = FALSE;
    for (CLBeacon *beacon in beacons) {
        if (beacon.rssi != 0) {
            BOOL addBeaconDevice = TRUE;
            NSInteger majorID;
            NSInteger minorID;
            NSString *beaconUUID;
            
//            NSLog(@"beaconDeviceList count: %zd", beaconDeviceList.count);
            for (BeaconDevice *device in beaconDeviceList) {
//                NSLog(@"device.beaconUUID: %@", device.beaconUUID);
                if ([device.beaconUUID isEqualToString:[beacon.proximityUUID UUIDString]]) {
                    device.beaconRssi = [NSNumber numberWithLong:beacon.rssi];
                    addBeaconDevice = FALSE;
                    majorID = [device.beaconMajor integerValue];
                    minorID = [device.beaconMinor integerValue];
                    beaconUUID = device.beaconUUID;
                    break;
                }
            }
            
            if (addBeaconDevice) {
                NSLog(@"addBeaconDevice");
                BeaconDevice *beaconDevice = [[BeaconDevice alloc] init];
                [beaconDevice setBeaconUUID:[beacon.proximityUUID UUIDString]];
                [beaconDevice setBeaconRssi:[NSNumber numberWithLong:beacon.rssi]];
                [beaconDevice setBeaconMajor:beacon.major];
                [beaconDevice setBeaconMinor:beacon.minor];
                [beaconDeviceList addObject:beaconDevice];
                loadLockDataNow = TRUE;
            } else {
//                NSLog(@"searchBeaconInLockData");
//                NSLog(@"lockDataCount: %zd", lockDataList.count);
                for (LockData *lockData in lockDataList) {
                    if ([lockData.beaconUUID isEqualToString:[beacon.proximityUUID UUIDString]] && [lockData.beaconMajor isEqualToNumber:beacon.major] && [lockData.beaconMinor isEqualToNumber:beacon.minor]) {
                        if ([lockData.inDatabase boolValue] && isInHome) {
                            [beaconDelegate beaconWithinRange:lockData.bleUUID];
                            break;
                        }
                    }
                }
            }
//            NSLog(@"beaconDevice UUID: %@", [beacon.proximityUUID UUIDString]);
//            NSLog(@"beaconDevice RSSI: %zd", beacon.rssi);
//            NSLog(@"beaconDeviceList count: %zd", [beaconDeviceList count]);
        }
    }
    
    if (loadLockDataNow) {
        [beaconDelegate getBleDeviceData];
    }
}

- (void)performActionForEvent:(NSString *)event withIdentifier:(NSString *)identifier {
    NSLog(@"performActionForEvent");
//    NSString *regionUUIDFromIdentifier = [self getRegionUUIDFromIdentifier:identifier];
    
}

@end
