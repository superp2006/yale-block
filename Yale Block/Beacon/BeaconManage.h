//
//  BeaconManage.h
//  Yale Block
//
//  Created by Peter on 10/13/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol BeaconManageDelegate <NSObject>

@optional
- (void) getBleDeviceData;
- (void) reloadLockDataTable;
- (void) beaconWithinRange:(NSString *) bleUUID;

@end

@interface BeaconManage : NSObject <CLLocationManagerDelegate>

+ (id) sharedInstance;

@property (nonatomic, assign) id<BeaconManageDelegate> beaconDelegate;

- (void) loadSavedBeacons:(NSArray *) beaconDevices addBleDevices:(NSArray *) bleDevices;
- (void) startMonitoringBeacon;
- (void) stopMonitoringBeacon;
- (void) loadLockData;

@property (strong, nonatomic) NSMutableArray *foundBeacons;
@property (strong, nonatomic) NSMutableArray *beaconList;
@property (strong, nonatomic) NSMutableArray *beaconDeviceList;
@property (strong, nonatomic) NSMutableArray *bleDeviceList;
@property (strong, nonatomic) NSMutableArray *lockDataList;

@property BOOL isInLockTable;
@property BOOL isInHome;

@end
