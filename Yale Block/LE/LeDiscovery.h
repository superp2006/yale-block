//
//  LeDiscovery.h
//  Yale Block
//
//  Created by Peter on 10/5/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreData/CoreData.h>
#import "LeLockService.h"

@protocol LeDiscoveryDelegate <NSObject>

@optional
- (void) discoveryDidRefresh;
//- (void) discoveryStatePoweredOff;
//- (void) discoveryRemoveLock:(NSString *)deviceName;
//- (void) discoveryAddLock:(NSString *)deviceName;

@end

@interface LeDiscovery : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

+ (id) sharedInstance;

@property (nonatomic, assign) id<LeDiscoveryDelegate> discoveryDelegate;
@property (nonatomic, assign) id<LeLockServiceProtocol> peripheralDelegate;

- (void) loadSavedDevices:(NSArray *) lockDevices;
- (void) startScanningForUUIDString:(NSString *)uuidString;
- (void) stopScanning;

- (void) connectPeripheral:(CBPeripheral*)peripheral;
- (void) disconnectPeripheral:(CBPeripheral*)peripheral;
- (void) connectBleDevice:(NSString *) bleDeviceID;
- (void) disconnectBleDevice:(NSString *) bleDeviceID;

@property (retain, nonatomic) NSMutableArray *foundPeripherals;
@property (retain, nonatomic) NSMutableArray *connectedServices;
@property (strong, nonatomic) NSMutableArray *lockList;
@property (strong, nonatomic) NSMutableArray *bleDeviceList;
//@property (strong, nonatomic) NSMutableArray *lockDataList;


@property (assign) NSInteger lockDoorCount;
@property BOOL isLockConnecting;
@property BOOL isLockTableLoaded;
@property BOOL isInHome;
@property BOOL isBleConnecting;
@property BOOL isOpenLock;
@property BOOL isCloseLock;
@property BOOL isCloseLockTimeStarted;
@property BOOL isInBackground;

@end
