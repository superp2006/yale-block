//
//  LeDiscovery.m
//  Yale Block
//
//  Created by Peter on 10/5/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "LeDiscovery.h"
#import "LockInfo.h"
#import "BleDevice.h"
#import "BeaconDevice.h"
#import "LockData.h"

@interface LeDiscovery() {
    CBCentralManager    *centralManager;
    BOOL				pendingInit;
}

@property (nonatomic) NSTimer *lockTimer;
@end

@implementation LeDiscovery

@synthesize lockTimer;
@synthesize foundPeripherals;
@synthesize connectedServices;
@synthesize discoveryDelegate;
@synthesize peripheralDelegate;
@synthesize isLockTableLoaded;
@synthesize lockList;
@synthesize bleDeviceList;
@synthesize isInHome;
@synthesize isBleConnecting;
@synthesize isOpenLock;
@synthesize isCloseLock;
@synthesize isCloseLockTimeStarted;
@synthesize isInBackground;
@synthesize isLockConnecting;
@synthesize lockDoorCount;
//@synthesize lockDataList;

#pragma mark -
#pragma mark Init

+ (id) sharedInstance {
    static LeDiscovery *this = nil;
    
    if (!this)
        this = [[LeDiscovery alloc] init];
    
    return this;
}

- (id) init
{
    self = [super init];
    if (self) {
        pendingInit = YES;
        centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
        
        foundPeripherals = [[NSMutableArray alloc] init];
        connectedServices = [[NSMutableArray alloc] init];
        lockList = [[NSMutableArray alloc] init];
        bleDeviceList = [[NSMutableArray alloc] init];
        
        lockDoorCount = 0;
        isLockTableLoaded = FALSE;
        isInHome = FALSE;
        isBleConnecting = FALSE;
        isOpenLock = FALSE;
        isCloseLock = FALSE;
        isInBackground = FALSE;
        isCloseLockTimeStarted = FALSE;
        isLockConnecting = FALSE;
        
        [self startLockDoorTimer];
    }
    return self;
}

- (void) loadSavedDevices:(NSArray *) lockDevices {
    NSArray *peripheralList;
    
    [self clearDevices];
    
    if (![lockDevices isKindOfClass:[NSArray class]]) {
        NSLog(@"No stored array to load");
        return;
    }
    
    NSLog(@"lockDevices count: %zd",[lockDevices count]);
    if ([lockDevices count] > 0) {
        for (LockInfo *lockInfo in lockDevices) {            
            BleDevice *bleDevice = [[BleDevice alloc] init];
            bleDevice.deviceName = lockInfo.lockID;
            bleDevice.deviceUUID = lockInfo.lockUUID;
            bleDevice.deviceRssi = 0;
            
            [bleDeviceList addObject:bleDevice];
            
            CBUUID *deviceID = [CBUUID UUIDWithString:lockInfo.lockUUID];
            peripheralList = [centralManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:(id)deviceID]];
        }
        
        self.lockList = [NSMutableArray arrayWithArray:lockDevices];
        self.foundPeripherals = [NSMutableArray arrayWithArray:peripheralList];
        
    }
}

//- (void) addLockData:(NSArray *) beaconDeviceList {
//
//    NSLog(@"lockDevices count: %zd",[beaconDeviceList count]);
//    if ([beaconDeviceList count] <= 0) {
//        NSLog(@"No beacon data loaded");
//        return;
//    }
//    
//    if ([bleDeviceList count] <= 0) {
//        NSLog(@"No ble data loaded");
//        return;
//    }    
//    
//    [lockDataList removeAllObjects];
//    
//    for (BeaconDevice *beacon in beaconDeviceList) {
//        for (BleDevice *ble in bleDeviceList) {
//            long beaconRssi = [beacon.beaconRssi longValue];
//            long bleRssi = [ble.deviceRssi longValue];
//            
//            if (bleRssi < -65) {
//                NSLog(@"Lock is not within range");
//                break;
//            }
//            
//            long rssiA = (beaconRssi - 5);
//            long rssiB = (beaconRssi + 5);
//            
//            NSLog(@"beaconRssi - 5: %zd", rssiA);
//            NSLog(@"beaconRssi + 5: %zd", rssiB);
//            NSLog(@"bleRssi: %zd", bleRssi);
//            
//            if ((rssiA < bleRssi) && (rssiB > bleRssi)) {
//                LockData *lockData = [[LockData alloc] init];
//                lockData.bleID = ble.deviceName;
//                lockData.bleUUID = ble.deviceUUID;
//                lockData.beaconUUID = beacon.beaconUUID;
//                lockData.beaconMajor = beacon.beaconMajor;
//                lockData.beaconMinor = beacon.beaconMinor;
//                NSLog(@"LockData bleID: %@", lockData.bleID);
//                NSLog(@"LockData bleUUID: %@", lockData.bleUUID);
//                NSLog(@"LockData beaconUUID: %@", lockData.beaconUUID);
//                
//                [lockDataList addObject:lockData];
//                break;
//            }
//        }
//    }
//    
//}

- (void) addSavedDevice:(LockInfo *)lockInfo {
    if (![lockInfo isKindOfClass:[LockInfo class]]) {
        NSLog(@"No lock to add");
        return;
    }
    
    [self.lockList addObject:lockInfo];
}

#pragma mark -
#pragma mark Discovery
- (void) startScanningForUUIDString:(NSString *)uuidString {
    NSArray *ServiceuUUID = @[[CBUUID UUIDWithString:@"FFF0"],
                     [[NSUUID alloc] initWithUUIDString:@"955A1523-0FE2-F5AA-A094-84B8D4F300AD"],
                     [[NSUUID alloc] initWithUUIDString:@"955A1523-0FE2-F5AA-A094-84B8D4F3E8AD"]];
    
    NSLog(@"startScanningForUUIDString: %@", ServiceuUUID);
    
    [centralManager scanForPeripheralsWithServices:ServiceuUUID options:nil];
}

- (void) stopScanning {
    NSLog(@"stopScanning");
    [centralManager stopScan];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
//    NSLog(@"didDiscoverPeripheral name=%@ rssi=%zd", peripheral.name, RSSI);
    if (![foundPeripherals containsObject:peripheral]) {
        [foundPeripherals addObject:peripheral];
//        if (self.isLockTableLoaded) {
//            [discoveryDelegate discoveryDidRefresh];
//        }
    }
    
    BOOL addBleDevice = TRUE;
    
    for (BleDevice *device in bleDeviceList) {
        if ([device.deviceName isEqualToString:peripheral.name]) {
            device.deviceRssi = RSSI;
            addBleDevice = FALSE;
            break;
        }
    }
    
    if (addBleDevice) {
        BleDevice *bleDevice = [[BleDevice alloc] init];
        
        [bleDevice setDeviceName:peripheral.name];
        [bleDevice setDeviceRssi:RSSI];
        [bleDevice setDeviceUUID:[peripheral.identifier UUIDString]];

        [bleDeviceList addObject:bleDevice];
    }
    
//    NSLog(@"bleDeviceList: %@", bleDeviceList);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadBeacon" object:nil];
}

#pragma mark -
#pragma mark Connection/Disconnection

- (void) connectBleDevice:(NSString *)bleDeviceID {
    BOOL run = FALSE;
    
    if (isInHome) {
        if (!isInBackground) {
            run = TRUE;
        } else if (isInBackground && isOpenLock) {
            run = TRUE;
        } else if (isCloseLock) {
            run = TRUE;
        } else if (isLockConnecting) {
            run = FALSE;
        } else {
            run = FALSE;
        }
    }
    
    if (run) {
        CBUUID *deviceID = [CBUUID UUIDWithString:bleDeviceID];
        NSArray *peripheralList = [centralManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:(id)deviceID]];
        CBPeripheral *peripheral = peripheralList.firstObject;
        if (peripheral.state != CBPeripheralStateConnected) {
            [centralManager connectPeripheral:peripheral options:nil];
        }
    }
}

- (void) disconnectBleDevice:(NSString *)bleDeviceID {
    if (isInHome) {
        CBUUID *deviceID = [CBUUID UUIDWithString:bleDeviceID];
        NSArray *peripheralList = [centralManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:(id)deviceID]];
        CBPeripheral *peripheral = peripheralList.firstObject;
        
        [centralManager cancelPeripheralConnection:peripheral];
    }
}

- (void) connectPeripheral:(CBPeripheral*)peripheral {
    
    if (peripheral.state != CBPeripheralStateConnected) {
        [centralManager connectPeripheral:peripheral options:nil];
    }
}

- (void) disconnectPeripheral:(CBPeripheral*)peripheral {
    
    [centralManager cancelPeripheralConnection:peripheral];
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"didConnectPeripheral: %@", peripheral.name);
    LeLockService *service	= nil;
    isLockConnecting = FALSE;
    /* Create a service instance. */
    service = [[LeLockService alloc] initWithPeripheral:peripheral controller:peripheralDelegate];
    [service setIsInBackground:isInBackground];
    [service setIsOpenLock:isOpenLock];
    [service setIsCloseLock:isCloseLock];
    [service setIsInHome:isInHome];
    [service start];
    
    for (LockInfo *lockInfo in lockList) {
        NSLog(@"lockInfo.lockID: %@", lockInfo.lockID);
        if ([lockInfo.lockID isEqualToString:peripheral.name]) {
            [service setLockName:lockInfo.lockName];
            break;
        }
    }

    if (![connectedServices containsObject:service])
        [connectedServices addObject:service];
    
    if ([foundPeripherals containsObject:peripheral])
        [foundPeripherals removeObject:peripheral];

//    [peripheralDelegate alarmServiceDidChangeStatus:service];
//    [discoveryDelegate discoveryDidRefresh];
}

- (void) centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    isLockConnecting = FALSE;
    NSLog(@"Attempted connection to peripheral %@ failed: %@", [peripheral name], [error localizedDescription]);
}

- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didDisconnectPeripheral");
    LeLockService	*service	= nil;
    
    for (service in connectedServices) {
        if ([service peripheral] == peripheral) {
            NSLog(@"remove peripheral %@ from service", peripheral.name);
            [connectedServices removeObject:service];
//            [peripheralDelegate alarmServiceDidChangeStatus:service];
            break;
        }
    }
    
    [discoveryDelegate discoveryDidRefresh];
}

- (void) clearDevices {
    LeLockService	*service;
    [foundPeripherals removeAllObjects];
    
    for (service in connectedServices) {
        [service reset];
    }
    [connectedServices removeAllObjects];
}

- (void) centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        NSArray *ServiceuUUID = @[[CBUUID UUIDWithString:@"FFF0"],
                                  [[NSUUID alloc] initWithUUIDString:@"955A1523-0FE2-F5AA-A094-84B8D4F300AD"],
                                  [[NSUUID alloc] initWithUUIDString:@"955A1523-0FE2-F5AA-A094-84B8D4F3E8AD"]];
        
        NSLog(@"startScanningForUUIDString: %@", ServiceuUUID);
        [centralManager scanForPeripheralsWithServices:ServiceuUUID options:nil];
    } else if (central.state == CBCentralManagerStatePoweredOff) {
        [self clearDevices];
    }
    
}

#pragma mark - Lock Timer

- (void) startLockDoorTimer {
    NSLog(@"startLockDoorTimer");
    lockTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(lockDoorEvent) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:lockTimer forMode:NSRunLoopCommonModes];
}

- (void) lockDoorEvent {
    if (isCloseLockTimeStarted) {
        NSLog(@"lockDoorEvent: count=%zd", lockDoorCount);
        if (lockDoorCount < 5) {
            lockDoorCount++;
        } else {
            lockDoorCount = 0;
            isCloseLockTimeStarted = FALSE;
            isCloseLock = TRUE;
        }
    }
}
@end
