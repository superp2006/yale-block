//
//  LeLockService.m
//  Yale Block
//
//  Created by Peter on 10/4/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import "LeLockService.h"

NSString *kLockServiceUUIDString = @"FFF0";
NSString *kRxCharacteristicUUIDString = @"FFF1";
NSString *kTxCharacteristicUUIDString = @"FFF2";

@interface LeLockService() {
@private
    CBPeripheral		*servicePeripheral;
    CBService			*lockService;
    CBCharacteristic    *txCharacteristic;
    CBCharacteristic	*rxCharacteristic;
    
    CBUUID              *txCharacteristicUUID;
    CBUUID              *rxCharacteristicUUID;
    
    id<LeLockServiceProtocol> peripheralDelegate;
}

@end

@implementation LeLockService

@synthesize peripheral = servicePeripheral;
@synthesize lockName;
@synthesize isReady;
@synthesize isOpenLock;
@synthesize isCloseLock;
@synthesize isInBackground;
@synthesize isInHome;

#pragma mark -
#pragma mark Init
- (id) initWithPeripheral:(CBPeripheral *)peripheral controller:(id<LeLockServiceProtocol>)controller {
    self = [super init];
    if (self) {
        servicePeripheral = peripheral;
        [servicePeripheral setDelegate:self];
        peripheralDelegate = controller;
        
        rxCharacteristicUUID = [CBUUID UUIDWithString:kRxCharacteristicUUIDString];
        txCharacteristicUUID = [CBUUID UUIDWithString:kTxCharacteristicUUIDString];
        isReady = FALSE;
        isOpenLock = FALSE;
        isCloseLock = FALSE;
        isInBackground = FALSE;
        isInHome = FALSE;
        
    }
    return self;
}

- (void) reset {
    if (servicePeripheral) {
//        [servicePeripheral release];
        servicePeripheral = nil;
    }
}

#pragma mark -
#pragma mark Service interaction
- (void) start {
    CBUUID	*lockServiceUUID = [CBUUID UUIDWithString:kLockServiceUUIDString];
    NSArray	*serviceArray = [NSArray arrayWithObjects:lockServiceUUID, nil];
    
    [servicePeripheral discoverServices:serviceArray];
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"didDiscoverServices");
    NSArray *services = nil;
    NSArray	*uuids = [NSArray arrayWithObjects:txCharacteristicUUID,
                                                rxCharacteristicUUID,
                                                nil];
    
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong Peripheral.\n");
        return ;
    }
    
    if (error != nil) {
        NSLog(@"Error %@\n", error);
        return ;
    }
    
    services = [peripheral services];
    if (!services || ![services count]) {
        return ;
    }
    
    lockService = nil;
    
    for (CBService *service in services) {
        if ([[service UUID] isEqual:[CBUUID UUIDWithString:kLockServiceUUIDString]]) {
            lockService = service;
            break;
        }
    }
    
    if (lockService) {
        [peripheral discoverCharacteristics:uuids forService:lockService];
    }
    
    if (isInHome) {
        [peripheralDelegate reloadTable];
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error; {
    NSLog(@"didDiscoverCharacteristicsForServices");
    NSArray	*characteristics = [service characteristics];
    CBCharacteristic *characteristic;
    
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong Peripheral.\n");
        return ;
    }
    
    if (service != lockService) {
        NSLog(@"Wrong Service.\n");
        return ;
    }
    
    if (error != nil) {
        NSLog(@"Error %@\n", error);
        return ;
    }
    
    NSLog(@"lockStatus: isOpenLock=%d isCloseLock=%d isInBackground=%d", isOpenLock, isCloseLock, isInBackground);
    
    for (characteristic in characteristics) {
        if ([[characteristic UUID] isEqual:txCharacteristicUUID]) {
            NSLog(@"Discovered TX Characteristic");
            txCharacteristic = characteristic;
            isReady = TRUE;
            if (isInBackground && isOpenLock && !isCloseLock) {
                [self unlockDoor];
                isOpenLock = FALSE;
                [peripheralDelegate unLockDoorInBackground];
            } else {
                if (isCloseLock) {
                    [self lockDoor];
                    isCloseLock = FALSE;
                }
            }
            [self performSelector:@selector(cancelLockConnection) withObject:nil afterDelay:8.0];
            
        } else if ([[characteristic UUID] isEqual:rxCharacteristicUUID]) {
            NSLog(@"Discovered RX Characteristic");
            rxCharacteristic = characteristic;
            [peripheral setNotifyValue:YES forCharacteristic:rxCharacteristic];
        }
    }
}

- (void) cancelLockConnection {
    NSLog(@"CancelLockConnection");
    [peripheralDelegate disconnectLockNow:[self.peripheral.identifier UUIDString]];
}

#pragma mark -
#pragma mark Characteristics interaction

- (void) lockDoor {
    NSLog(@"lockDoor");
    Byte sendLockx[]={0x02,0xF2,0x01,0x12,0x34,0x56,0x78};
    NSData *sendData = [[NSData alloc] initWithBytes:sendLockx length:sizeof(sendLockx)];
    [self writeLockData:sendData];
    if (isInBackground) {
        [peripheralDelegate didLockDoor:[self.peripheral.identifier UUIDString] lockName:self.lockName];
    }
}

- (void) unlockDoor {
    NSLog(@"unlockDoor");
    Byte sendUNlockx[]={0x02,0xF1,0x01,0x12,0x34,0x56,0x78};
    NSData *sendData = [[NSData alloc] initWithBytes:sendUNlockx length:sizeof(sendUNlockx)];
    [self writeLockData:sendData];
    if (isInBackground) {
        [peripheralDelegate didUnlockDoor:[self.peripheral.identifier UUIDString] lockName:self.lockName];
    }
}

- (void) writeLockData:(NSData *)action {
    NSLog(@"writeLockData >>>>>>>>> isReady=%s", isReady ? "YES" : "NO");
//    NSLog(@"isReady: %s", isReady ? "true" : "false");
    if (isReady) {
        if (!servicePeripheral) {
            NSLog(@"Not connected to a peripheral");
            return ;
        }
        
        if (!txCharacteristic) {
            NSLog(@"No valid rx characteristic");
            return;
        }
        
        if ((txCharacteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) != 0) {
            NSLog(@"writeDataWithoutResponseNow");
            [servicePeripheral writeValue:action forCharacteristic:txCharacteristic type:CBCharacteristicWriteWithoutResponse];
        } else if ((txCharacteristic.properties & CBCharacteristicPropertyWrite) != 0) {
            NSLog(@"writeDataWithResponseNow");
            [servicePeripheral writeValue:action forCharacteristic:txCharacteristic type:CBCharacteristicWriteWithResponse];
        }
    }
    
    
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (peripheral != servicePeripheral) {
        NSLog(@"Wrong peripheral\n");
        return ;
    }
    
    if ([error code] != 0) {
        NSLog(@"Error %@\n", error);
        return ;
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    NSLog(@"didWriteValueForCharacteristic");
    NSLog(@"characteristic UUID: %@", characteristic.UUID);
    NSLog(@"Error: %@", error);
}

@end