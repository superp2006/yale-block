//
//  LeLockService.h
//  Yale Block
//
//  Created by Peter on 10/4/15.
//  Copyright © 2015 ardi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

extern NSString *kLockServiceUUIDString;
extern NSString *kRxCharacteristicUUIDString;
extern NSString *kTxCharacteristicUUIDString;

@class LeLockService;

@protocol LeLockServiceProtocol <NSObject>

@optional
- (void) reloadTable;
- (void) unLockDoorInBackground;
- (void) didUnlockDoor:(NSString *)uuid lockName:(NSString *)lockName;
- (void) didLockDoor:(NSString *)uuid lockName:(NSString *)lockName;
- (void) disconnectLockNow:(NSString *)uuid;

//- (void) lockDoor;
//- (void) unlockDoor;

@end

@interface LeLockService : NSObject <CBPeripheralDelegate>

@property (nonatomic, strong, readonly) CBPeripheral *peripheral;
@property (nonatomic, strong) NSString *lockName;
@property BOOL isReady;
@property BOOL isOpenLock;
@property BOOL isCloseLock;
@property BOOL isInBackground;
@property BOOL isInHome;

- (id) initWithPeripheral:(CBPeripheral *)peripheral controller:(id<LeLockServiceProtocol>)controller;
- (void) reset;
- (void) start;
- (void) lockDoor;
- (void) unlockDoor;
- (void) writeLockData:(NSData *)action;

@end
